﻿using UnityEngine;
using System.Collections;

public class FloorSwitch : Switch {

    public override void Awake() {
        base.Awake();

        isToggleable = true;
    }

    public override void Start() {
        base.Start();
    }

    protected override void OnActivePlayerChanged(Player p) {
        base.OnActivePlayerChanged(p);
    }

    protected override void OnTriggerExit(Collider other) {
        base.OnTriggerExit(other);
    }

    protected override void OnTriggerEnter(Collider other) {
        base.OnTriggerEnter(other);
    }

    protected override void OnPlayerGrabbed() {
        base.OnPlayerGrabbed();

        OnSwitchHit();
    }

    protected override void OnPlayerLeft() {
        base.OnPlayerLeft();

        OnSwitchRelease();
    }

    public override void OnSwitchHit() {
        base.OnSwitchHit();
    }

    public override void OnSwitchRelease() {
        base.OnSwitchRelease();
    }

    

    public override void Update() {
        base.Update();

        if (debug) {
            float distance = 0;
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, 1000)) {
                distance = hitInfo.distance;
            }

            Debug.DrawRay(transform.position, Vector3.down * distance, Color.red);
        }
    }
}
