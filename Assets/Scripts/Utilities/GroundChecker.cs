﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class GroundChecker : MonoBehaviour {

    [HideInInspector]
    public UnityEvent OnGroundTriggerEvent;

	// Use this for initialization
	void Start () {
	
	}

    public void OnCollisionEnter(Collision collision) {
        if (OnGroundTriggerEvent != null) {
            OnGroundTriggerEvent.Invoke();
        }
    }


	// Update is called once per frame
	void Update () {
	
	}
}
