﻿using UnityEngine;
using System.Collections;
public class Block {

    // 1 divided by the number of tiles per row/column in tile sheet
    private const float TILE_SIZE = 0.25f;

    //Base block constructor
    public Block() {
    }

    public virtual bool IsSolid(Direction direction) {
        bool rVal = false;
        switch (direction) {
            case Direction.North:
                rVal = true;
                break;
            case Direction.East:
                rVal = true;
                break;
            case Direction.South:
                rVal = true;
                break;
            case Direction.West:
                rVal = true;
                break;
            case Direction.Up:
                rVal = true;
                break;
            case Direction.Down:
                rVal = true;
                break;
            default:
                rVal = true;
                break;
        }

        return rVal;
    }

    public virtual MeshData Blockdata(Chunk chunk, int x, int y, int z, MeshData meshData) {

        meshData.useRenderDataForCol = true;

        if (!chunk.GetBlockFaceIsSolid(x, y + 1, z, Direction.Down)) {
            meshData = FaceDataUp(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlockFaceIsSolid(x, y - 1, z, Direction.Up)) {
            meshData = FaceDataDown(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlockFaceIsSolid(x, y, z + 1, Direction.South)) {
            meshData = FaceDataNorth(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlockFaceIsSolid(x, y, z - 1, Direction.North)) {
            meshData = FaceDataSouth(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlockFaceIsSolid(x + 1, y, z, Direction.West)) {
            meshData = FaceDataEast(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlockFaceIsSolid(x - 1, y, z, Direction.East)) {
            meshData = FaceDataWest(chunk, x, y, z, meshData);
        }

        return meshData;
    }

    protected virtual MeshData FaceDataUp(Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        
        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.Up));
        return meshData;
    }

    protected virtual MeshData FaceDataDown(Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.Down));
        return meshData;
    }

    protected virtual MeshData FaceDataNorth(Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.North));
        return meshData;
    }

    protected virtual MeshData FaceDataEast(Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.East));
        return meshData;
    }

    protected virtual MeshData FaceDataSouth(Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.South));
        return meshData;
    }

    protected virtual MeshData FaceDataWest(Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.West));
        return meshData;
    }

    public struct Tile { 
        public int x; 
        public int y;
    }

    public virtual Tile TexturePosition(Direction direction) {
        Tile tile = new Tile();
        tile.x = 2;
        tile.y = 2;
        return tile;
    }

    public virtual Vector2[] FaceUVs(Direction direction) {
        Vector2[] UVs = new Vector2[4];
        Tile tilePos = TexturePosition(direction);

        UVs[0] = new Vector2(TILE_SIZE * tilePos.x + TILE_SIZE, TILE_SIZE * tilePos.y);
        UVs[1] = new Vector2(TILE_SIZE * tilePos.x + TILE_SIZE, TILE_SIZE * tilePos.y + TILE_SIZE); 
        UVs[2] = new Vector2(TILE_SIZE * tilePos.x, TILE_SIZE * tilePos.y + TILE_SIZE);
        UVs[3] = new Vector2(TILE_SIZE * tilePos.x,TILE_SIZE * tilePos.y);

        return UVs;
    }

}