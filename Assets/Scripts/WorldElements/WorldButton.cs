﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class WorldButton : MonoBehaviour {

    protected BoxCollider boxCollider;

    public virtual void Awake() {
        boxCollider = this.GetComponent<BoxCollider>();
    }

	// Use this for initialization
	public virtual void Start () {
	
	}

    public virtual void OnMouseUpAsButton() {

    }
    
	
	// Update is called once per frame
    public virtual void Update() {
	
	}
}
