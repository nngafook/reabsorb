﻿using UnityEngine;
using System.Collections;

public class VoxelData  {

    private int[,] data = new int[,] { { 0, 1, 1 }, { 1, 1, 1 }, { 1, 1, 0 } };

    private DataCoordinate[] offsets = {
        new DataCoordinate(0, 0, 1),
        new DataCoordinate(1, 0, 0),
        new DataCoordinate(0, 0, -1),
        new DataCoordinate(-1, 0, 0),
        new DataCoordinate(0, 1, 0),
        new DataCoordinate(0, -1, 0)
    };

    public int Width { get { return data.GetLength(0); } }
    public int Depth { get { return data.GetLength(1); } }
    
    public int GetCell(int x, int z) {
        return data[x, z];
    }

    public int GetNeighbour(int x, int z, Direction dir) {
        int rVal = 0;
        DataCoordinate offsetToCheck = offsets[(int)dir];
        DataCoordinate neighbourCoord = new DataCoordinate(x + offsetToCheck.x, 0 + offsetToCheck.y, z + offsetToCheck.z);

        if (neighbourCoord.x < 0 || neighbourCoord.x >= Width || neighbourCoord.y != 0 || neighbourCoord.z < 0 || neighbourCoord.z >= Depth) {
            rVal = 0;
        }
        else {
            rVal = GetCell(neighbourCoord.x, neighbourCoord.z);
        }

        return rVal;
    }

    private struct DataCoordinate {
        public int x;
        public int y;
        public int z;

        public DataCoordinate(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

}

public enum Direction {
    North,
    East,
    South,
    West,
    Up,
    Down
}