﻿using UnityEngine;
using System.Collections;

public class GlowReceiver : MonoBehaviour {

    [SerializeField]
    private GlowColor glowColor;
    [SerializeField]
    private bool isShowing = true;

    public GameObject hiddenObjectsParent;
    public Collider mainCollider;

    public bool IsShowing { get { return isShowing; } }

	// Use this for initialization
	void Start () {
        hiddenObjectsParent.SetActive(isShowing);
        mainCollider.enabled = isShowing;
	}

    private void CheckSphere() {
        Collider[] hitColliders = Physics.OverlapBox(transform.position, new Vector3(15, 5, 15));
        bool stillShowing = false;
        for (int i = 0; i < hitColliders.Length; i++) {
            if (hitColliders[i].gameObject.tag == "GlowZone") {
                GlowZone gz = hitColliders[i].GetComponent<GlowZone>();
                if (gz != null && IsShowing && gz.GlowColor == glowColor) {
                    stillShowing = true;
                    break;
                }
            }
        }
        if (!stillShowing) {
            GlowExit();
        }
    }

    public void GlowEnter(GlowColor gc) {
        if (!isShowing && gc == glowColor) {
            isShowing = true;

            hiddenObjectsParent.SetActive(isShowing);
            mainCollider.enabled = isShowing;
        }
    }

    private void GlowExit() {
        if (isShowing) {
            isShowing = false;

            hiddenObjectsParent.SetActive(isShowing);
            mainCollider.enabled = isShowing;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (isShowing) {
            CheckSphere();
        }
	}
}
