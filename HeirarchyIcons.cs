﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
[InitializeOnLoad]
public class HierarchyIcons {
    //static Texture2D textureManager;
    static Texture2D playerHeirarchyIcon;
    static Texture2D databaseHeirarchyIcon;
    static Texture2D mothershipIcon;
    static Texture2D textureBg;
    static Texture2D selectedBG;
    /// <summary>
    /// Initializer <see cref="HierarchQuickSetActive"/> class.
    /// </summary>
    static HierarchyIcons() {
        //textureManager = AssetDatabase.LoadAssetAtPath("Assets/Art/Textures/TextureManager.png", typeof(Texture2D)) as Texture2D;
        mothershipIcon = AssetDatabase.LoadAssetAtPath("Assets/Art/Textures/MothershipIconTwo.png", typeof(Texture2D)) as Texture2D;
        playerHeirarchyIcon = AssetDatabase.LoadAssetAtPath("Assets/Art/Textures/PlayerHeirarchyIcon.png", typeof(Texture2D)) as Texture2D;
        databaseHeirarchyIcon = AssetDatabase.LoadAssetAtPath("Assets/Art/Textures/DatbaseHeirarchyIcon.png", typeof(Texture2D)) as Texture2D;
        textureBg = AssetDatabase.LoadAssetAtPath("Assets/Art/Textures/GrayEditorTexture.png", typeof(Texture2D)) as Texture2D;
        selectedBG = AssetDatabase.LoadAssetAtPath("Assets/Art/Textures/BlueEditorSelectedTexture.png", typeof(Texture2D)) as Texture2D;
        EditorApplication.hierarchyWindowItemOnGUI += HierarchWindowOnGUI;
    }


    /// <summary>
    /// Editor delegate callback
    /// </summary>
    /// <param craftableName="instanceID">İnstance id.</param>
    /// <param craftableName="selectionRect">Selection rect.</param>
    static void HierarchWindowOnGUI(int instanceID, Rect selectionRect) {
        Object o = EditorUtility.InstanceIDToObject(instanceID);
        if (!o)
            return;

        GameObject g = (GameObject)o as GameObject;
        if (g.activeSelf) {
            Rect r = new Rect(selectionRect);
            r.x = r.width - 25;
            r.width = 18;

            GUIStyle label2 = new GUIStyle(GUI.skin.GetStyle("label"));

            if (Selection.activeGameObject == g) {
                if (g.GetComponent<CanvasGroup>()) {
                    //Rect ToggleRect = r;
                    //ToggleRect.x = selectionRect.width - 8;
                    //bool visible = (g.GetComponent<CanvasGroup>().alpha == 1) ? true : false;
                    //visible = (GUI.Toggle(ToggleRect, visible, string.Empty));
                    //g.GetComponent<CanvasGroup>().alpha = visible.AsInt();
                    //g.GetComponent<CanvasGroup>().blocksRaycasts = visible;
                    //g.GetComponent<CanvasGroup>().interactable = visible;
                }
            }

            if (g.name.Contains("Player")) {
                label2.normal.textColor = (Selection.activeObject == g) ? Color.white : new Color(0.1686275f, 0.5843138f, 0.7921569f, 1f);
                label2.normal.background = (Selection.activeObject == g) ? selectedBG : textureBg;
                GUI.Box(selectionRect, g.name, label2);
                GUI.Label(r, playerHeirarchyIcon);
            }

            if (g.name.Contains("Mothership")) {
                label2.normal.textColor = (Selection.activeObject == g) ? Color.white : new Color(0.4470588f, 0.2352941f, 0.9843137f, 1f);
                label2.normal.background = (Selection.activeObject == g) ? selectedBG : textureBg;
                label2.fontStyle = FontStyle.Bold;
                GUI.Box(selectionRect, g.name, label2);
                GUI.Label(r, mothershipIcon);
            }

            if (g.name.Contains("Canvas")) {
                label2.normal.textColor = (Selection.activeObject == g) ? Color.white : Color.red;
                label2.normal.background = (Selection.activeObject == g) ? selectedBG : textureBg;
                GUI.Box(selectionRect, g.name, label2);
            }

            if (g.name.Contains("Window")) {
                label2.normal.textColor = (Selection.activeObject == g) ? Color.white : CustomColor.GetColor(ColorName.PURPLE_PLUM);
                label2.normal.background = (Selection.activeObject == g) ? selectedBG : textureBg;
                GUI.Box(selectionRect, g.name, label2);
            }

            if ((g.name.Contains("Database")) || (g.name.Contains("Utility"))) {
                label2.normal.textColor = (Selection.activeObject == g) ? Color.white : Color.magenta;
                label2.normal.background = (Selection.activeObject == g) ? selectedBG : textureBg;
                GUI.Box(selectionRect, g.name, label2);
                GUI.Label(r, databaseHeirarchyIcon);
            }
            if (GUI.changed) {
                if (!EditorSceneManager.GetActiveScene().isDirty) {
                    EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                }
            }
        }
    }
}