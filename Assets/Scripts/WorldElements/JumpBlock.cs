﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class JumpBlock : WorldElement {

    public Vector3 jumpToPosition;

    public override void SwitchActivate() {
        base.SwitchActivate();
        SetActivated(true);
    }

    public override void SwitchDeactivate() {
        base.SwitchDeactivate();
        SetActivated(false);
    }

    public override void Trigger() {
        base.Trigger();

        jumpToPosition = new Vector3(jumpToPosition.x, activePlayer.transform.position.y, jumpToPosition.z);

        activePlayer.transform.DOJump(jumpToPosition, 5, 1, 0.75f).SetEase(Ease.Linear).OnComplete(OnJumpComplete);

        Vector3 normalizedJumpDirection = (jumpToPosition - activePlayer.transform.position).normalized;

        if (activePlayer.NormalizedForward() != normalizedJumpDirection) {
            activePlayer.RotateTo(normalizedJumpDirection);
        }
    }

    private void OnJumpComplete() {
        activePlayer.LandFromJump();
    }

}
