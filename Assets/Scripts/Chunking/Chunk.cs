﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public class Chunk : MonoBehaviour {

    public static int chunkSize = 16;
    public bool update = true;

    private Block[, ,] blocks;
    private MeshFilter filter;
    private MeshCollider coll;

    //Use this for initialization
    void Start() {
        filter = gameObject.GetComponent<MeshFilter>();
        coll = gameObject.GetComponent<MeshCollider>();

        //past here is just to set up an example chunk
        blocks = new Block[chunkSize, chunkSize, chunkSize];
        for (int x = 0; x < chunkSize; x++) {
            for (int y = 0; y < chunkSize; y++) {
                for (int z = 0; z < chunkSize; z++) {
                    blocks[x, y, z] = new BlockAir();
                }
            }
        }
        blocks[3, 5, 2] = new Block();
        blocks[4, 5, 2] = new BlockGrass();

        blocks[0, 0, 0] = new Block();
        blocks[1, 0, 0] = new Block();
        blocks[2, 0, 0] = new Block();
        blocks[0, 0, 1] = new Block();
        blocks[1, 0, 1] = new Block();
        blocks[2, 0, 1] = new Block();
        blocks[0, 0, 2] = new Block();
        blocks[1, 0, 2] = new Block();
        blocks[2, 0, 2] = new Block();
        
        UpdateChunk();
    }
    
    public Block GetBlock(int x, int y, int z) {
        Block rVal = null;

        if (x < 0 || x >= blocks.GetLength(0) || y < 0 || y >= blocks.GetLength(1) || z < 0 || z >= blocks.GetLength(2)) {
            rVal = null;
        }
        else {
            rVal = blocks[x, y, z];
        }

        return rVal;
    }

    /// <summary>
    /// Returns true if the face of the block is solid.
    /// Returns false if block is null
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    public bool GetBlockFaceIsSolid(int x, int y, int z, Direction direction) {
        bool rVal = false;
        Block block = GetBlock(x, y, z);
        
        if (block != null) {
            rVal = block.IsSolid(direction);
        }

        return rVal;
    }

    // Updates the chunk based on its contents
    private void UpdateChunk() {
        MeshData meshData = new MeshData();
        for (int x = 0; x < chunkSize; x++) {
            for (int y = 0; y < chunkSize; y++) {
                for (int z = 0; z < chunkSize; z++) {
                    meshData = blocks[x, y, z].Blockdata(this, x, y, z, meshData);
                }
            }
        }
        RenderMesh(meshData);
    }

    // Sends the calculated mesh information
    // to the mesh and collision components
    private void RenderMesh(MeshData meshData) {
        filter.mesh.Clear();
        filter.mesh.vertices = meshData.vertices.ToArray();
        filter.mesh.triangles = meshData.triangles.ToArray();

        filter.mesh.uv = meshData.uv.ToArray();
        filter.mesh.RecalculateNormals();

        coll.sharedMesh = null;
        Mesh mesh = new Mesh();
        mesh.vertices = meshData.colVertices.ToArray();
        mesh.triangles = meshData.colTriangles.ToArray();
        mesh.RecalculateNormals();

        coll.sharedMesh = mesh;
    }

    //Update is called once per frame
    void Update() {
        
    }

}