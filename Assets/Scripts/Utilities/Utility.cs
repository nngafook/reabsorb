﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

#pragma warning disable 0660
#pragma warning disable 0661
public static class Utility {

    /// <summary>
    /// Shuffles the original, does NOT make/return a copy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="array"></param>
    /// <param name="seed"></param>
    /// <returns></returns>
    public static T[] ShuffleArray<T>(T[] array, int seed) {
        System.Random prng = new System.Random(seed);

        for (int i = 0; i < array.Length - 1; i++) {
            int randomIndex = prng.Next(i, array.Length);
            T tempItem = array[randomIndex];
            array[randomIndex] = array[i];
            array[i] = tempItem;
        }

        return array;
    }

    /// <summary>
    /// Shuffles a copy and returns that, leaving the original the same
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="seed"></param>
    /// <returns></returns>
    public static List<T> ShuffleList<T>(List<T> list, int seed) {
        List<T> rVal = new List<T>(list);
        System.Random prng = new System.Random(seed);

        for (int i = 0; i < rVal.Count - 1; i++) {
            int randomIndex = prng.Next(i, rVal.Count);
            T tempItem = rVal[randomIndex];
            rVal[randomIndex] = rVal[i];
            rVal[i] = tempItem;
        }

        return rVal;
    }

    public static bool IsWithinGrid(int coordColumn, int coordRow, int gridWidth, int gridHeight) {
        return (((coordColumn >= 0) && (coordColumn < gridWidth)) && ((coordRow >= 0) && (coordRow < gridHeight)));
    }

    public static Vector2 MouseWorldPosition() {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public static Vector2 MousePositionFromCenterScreen() {
        Vector2 mousePos = Input.mousePosition;
        mousePos.x -= Screen.width / 2;
        mousePos.y -= Screen.height / 2;
        return mousePos;
    }

    public static float RoundToNearestNth(float value, float n) {
        float rVal = 0;

        rVal = Mathf.RoundToInt(value / n) * n;

        return rVal;
    }

    public static void WriteData(string data, string folderPath) {
        if (!string.IsNullOrEmpty(data)) {
            string path = Application.persistentDataPath + folderPath;
            if (!string.IsNullOrEmpty(path)) {
                using (FileStream fs = new FileStream(path, FileMode.Create)) {
                    using (StreamWriter writer = new StreamWriter(fs)) {
                        writer.Write(data);
                    }
                }
                //Debug.Log("Export Complete".Bold().Colored(Color.yellow));
            }
        }
    }

    public static string IDToName(string id, string suffixIDTag = "_id") {
        char c;
        List<char> chars = new List<char>();
        chars.Add(char.ToUpper(id[0]));
        for (int i = 1; i < id.Length - suffixIDTag.Length; i++) {
            c = id[i];
            if (char.IsUpper(c)) {
                chars.Add(' ');
                chars.Add(char.ToUpper(c));
            }
            else
                chars.Add(c);
        }

        return new string(chars.ToArray());
    }

    public static string EnumNameToReadable(string id) {
        string rVal = id.Replace('_', ' ');
        return ToCamelCase(rVal);
    }

    public static string ToCamelCase(string value) {
        value = value.ToLower();
        char[] array = value.ToCharArray();
        // Handle the first letter in the string.
        if (array.Length >= 1) {
            if (char.IsLower(array[0])) {
                array[0] = char.ToUpper(array[0]);
            }
        }
        // Scan through the letters, checking for spaces.
        // ... Uppercase the lowercase letters following spaces.
        for (int i = 1; i < array.Length; i++) {
            if (array[i - 1] == ' ') {
                if (char.IsLower(array[i])) {
                    array[i] = char.ToUpper(array[i]);
                }
            }
        }
        return new string(array);
    }

    public static string CamelCaseToReadable(string value) {
        StringBuilder sb = new StringBuilder(value.Length * 2);
        char[] charArray = value.ToCharArray();
        // Uppercase first letter
        sb.Append(char.ToUpper(charArray[0]));
        for (int i = 1; i < charArray.Length; i++) {
            if (char.IsUpper(charArray[i])) {
                if (char.IsLower(charArray[i - 1])) {
                    sb.Append(' ');
                }
            }
            sb.Append(charArray[i]);
        }

        return sb.ToString();
    }

    public static string CharacterIDToReadable(string value) {
        if (value[0] == 'e') {
            value = value.Substring(1);
        }
        return EnumNameToReadable(value);
    }

    public static Color RandomColor() {
        return new Color(UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f));
    }

    public static void Assert<T>(T value) {
        if (value == null) {
            Debug.Log("Asserted".Bold().Sized(14).Colored(CustomColor.GetColor(ColorName.ERROR_RED)));
            Debug.Break();
        }
    }

    /// <summary>
    /// This is for the EnumFromString method. So that a generic class can be made to instantiate objects with an enum type unknown at compile
    /// </summary>
    public enum EnumType {
        QuestionCategory,
        QuestionType,
        AnswerCategory,
        ColorAnswerValue
    }

    public static T EnumFromString<T>(EnumType type) {
        T rVal = (T)(object)null;

        //switch (type) {
        //    case EnumType.QuestionCategory:
        //        rVal = (T)(object)(typeof(QuestionCategory));
        //        break;
        //    case EnumType.QuestionType:
        //        rVal = (T)(object)(typeof(QuestionType));
        //        break;
        //    case EnumType.AnswerCategory:
        //        rVal = (T)(object)(typeof(AnswerCategory));
        //        break;
        //    case EnumType.ColorAnswerValue:
        //        rVal = (T)(object)(typeof(ColorAnswerValue));
        //        break;
        //    default:
        //        break;
        //}

        return rVal;
    }

    public static Texture2D GenerateTextureFromSprite(Sprite aSprite) {
        Rect rect = aSprite.rect;
        Texture2D tex = new Texture2D((int)rect.width, (int)rect.height);
        Color[] data = aSprite.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
        tex.SetPixels(data);
        tex.Apply(true);
        return tex;
    }

    public static string GenerateUniqueStringID() {
        string rVal = "";
        //int seed = Environment.TickCount;
        int seed = UnityEngine.Random.Range(1, 10000) * UnityEngine.Random.Range(1, 2000);
        System.Random prng = new System.Random(seed);
        int idLength = 18;
        StringBuilder sb = new StringBuilder(idLength);

        // Generate letters
        for (int i = 0; i < idLength; i++) {
            if (i % 2 == 0) {
                // Letters
                sb.Append((char)prng.Next(65, 91));
            }
            else {
                // Numbers
                int previousChar = sb[i - 1];
                int appendValue = 0;
                while (previousChar != 0) {
                    appendValue += previousChar % 10;
                    previousChar /= 10;
                    if (previousChar == 0 && appendValue > 9) {
                        previousChar = appendValue;
                        appendValue = 0;
                    }
                }

                appendValue += prng.Next(-appendValue, 10 - appendValue);
                sb.Append(appendValue);
            }
        }

        rVal = sb.ToString();
        return rVal;
    }

    public static string GenerateUID() {
        string rVal = "";
        StringBuilder sb = new StringBuilder();

        int epochTime = ((int)((DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds)) - (UnityEngine.Random.Range(1, 2000) * UnityEngine.Random.Range(1, 200));
        sb.Append(epochTime.ToString());
        sb.Append("-");

        rVal = GenerateUniqueStringID();
        sb.Append(rVal);
        rVal = sb.ToString();
        return rVal;
    }

    public static Color GetGlowColorValue(GlowColor gc) {
        Color rVal = Color.white;
        switch (gc) {
            case GlowColor.Red:
                rVal = Color.red;
                break;
            case GlowColor.Green:
                rVal = Color.green;
                break;
            case GlowColor.Blue:
                rVal = Color.blue;
                break;
            case GlowColor.Yellow:
                rVal = Color.yellow;
                break;
        }
        return rVal;
    }

}

#region CLASSES_AND_STRUCTS
[Serializable]
public class IntEvent : UnityEvent<int> { }

[Serializable]
public class StringEvent : UnityEvent<string> { }

[Serializable]
public class BoolEvent : UnityEvent<bool> { }

//[Serializable]
public class UID : IComparable<UID> {
    private static int factoryValue = 1;
    //[SerializeField]
    private int value = 0;

    public int Value { get { return this.value; } }

    public UID() {
        value = factoryValue;
        factoryValue++;
    }

    public int CompareTo(UID other) {
        return other.value.CompareTo(this.value);
    }
}


[Serializable]
public class RangedFloat {
    public float min = 0;
    public float max = 1;

    public RangedFloat() {
        min = 0;
        max = 1;
    }

    public RangedFloat(float minValue, float maxValue) {
        min = minValue;
        max = maxValue;
    }

    public float PercentageOfValue(float value) {
        return Mathf.RoundToInt((100 / (max - min)) * (value - min));
    }

    public void AddToRange(float value) {
        min += value;
        max += value;
    }

    public float Random {
        get {
            return UnityEngine.Random.Range(min, max);
        }
    }

    public int RandomInt {
        get {
            return Mathf.RoundToInt(UnityEngine.Random.Range(min, max));
        }
    }
}

/// <summary>
/// A float used in percentage calculations.
/// </summary>
[Serializable]
public class PercentageFloat {
    [Range(0, 100)]
    public float baseValue;

    public PercentageFloat() {
        baseValue = 50;
    }

    public PercentageFloat(float v) {
        baseValue = v;
    }

    public float BaseValue {
        get {
            return baseValue;
        }
    }

    public void AddToBase(float value) {
        baseValue += value;
    }

    public bool Success {
        get {
            float randomValue = UnityEngine.Random.Range(0, 100);
            return (randomValue <= baseValue);
        }
    }
}

public class CustomColor {

    private static Color soniPoop = new Color(0.6f, 0.5f, 0.1f, 1f);
    private static Color errorRed = new Color(1f, 0.1803922f, 0.1803922f, 1f);
    private static Color submitGreen = new Color(0.3f, 0.9f, 0.4f, 1f);
    private static Color robBlue = new Color(0.3f, 0.6f, 0.9f, 1f);
    private static Color purplePlum = new Color(0.5921569f, 0.1568628f, 0.4313726f, 1f);
    private static Color warningYellow = new Color(0.8196079f, 0.8823529f, 0.427451f, 1f);
    private static Color yallow = new Color(0.8313726f, 0.7411765f, 0.3647059f, 1f);
    private static Color buttonDisabledGray = new Color(0.2313726f, 0.2313726f, 0.2313726f, 1f);
    private static Color urgentUrange = new Color(1f, 0.7372549f, 0.2705882f, 1f);
    private static Color racyRed = new Color(0.945098f, 0.1921569f, 0.1921569f, 1f);
    private static Color prsPurple = new Color(0.4392157f, 0.4235294f, 0.8352941f, 1f);
    private static Color skyBlue = new Color(0.4235294f, 0.7294118f, 0.8352941f, 1f);
    private static Color skierBlue = new Color(0.2901961f, 0.6f, 0.8235294f, 1f);
    private static Color dullGray = new Color(0.6039216f, 0.6666667f, 0.6901961f, 1f);

    public static Color GetColor(ColorName color) {
        Color rVal = new Color();

        switch (color) {
            case ColorName.SONI_POOP:
                rVal = soniPoop;
                break;
            case ColorName.ERROR_RED:
                rVal = errorRed;
                break;
            case ColorName.SUBMIT_GREEN:
                rVal = submitGreen;
                break;
            case ColorName.ROB_BLUE:
                rVal = robBlue;
                break;
            case ColorName.PURPLE_PLUM:
                rVal = purplePlum;
                break;
            case ColorName.URGENT_URANGE:
                rVal = urgentUrange;
                break;
            case ColorName.WARNING_YELLOW:
                rVal = warningYellow;
                break;
            case ColorName.YALLOW:
                rVal = yallow;
                break;
            case ColorName.RACY_RED:
                rVal = racyRed;
                break;
            case ColorName.PRS_PURPLE:
                rVal = prsPurple;
                break;
            case ColorName.SKY_BLUE:
                rVal = skyBlue;
                break;
            case ColorName.SKIER_BLUE:
                rVal = skierBlue;
                break;
            case ColorName.DULL_GRAY:
                rVal = dullGray;
                break;
            case ColorName.BUTTON_DISABLED_GRAY:
                rVal = buttonDisabledGray;
                break;
        }

        return rVal;
    }
}

[Serializable]
public class StringStringKeyValue {

    public string key;
    public string value;

    public StringStringKeyValue(string k, string v) {
        key = k;
        value = v;
    }

}

[Serializable]
public class StringFloatKeyValue {

    public string key;
    public float value;

    public StringFloatKeyValue(string k, float v) {
        key = k;
        value = v;
    }

}
#endregion CLASSES_AND_STRUCTS


public enum ColorName {
    SONI_POOP,
    ERROR_RED,
    SUBMIT_GREEN,
    ROB_BLUE,
    PURPLE_PLUM,
    WARNING_YELLOW,
    YALLOW,
    URGENT_URANGE,
    RACY_RED,
    PRS_PURPLE,
    SKY_BLUE,
    DULL_GRAY,
    SKIER_BLUE,

    BUTTON_DISABLED_GRAY = 999
}

[Serializable]
public enum GlowColor {
    Red,
    Green,
    Blue,
    Yellow
}