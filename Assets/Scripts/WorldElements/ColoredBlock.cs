﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class ColoredBlock : MonoBehaviour {

    [HideInInspector]
    public BoolEvent OnColorChanged = new BoolEvent();

    [SerializeField]
    private bool isVisible = false;

    public GlowColor glowColor;

    public Renderer modelRenderer;
    public MeshCollider meshCollider;

    public bool IsVisible { get { return isVisible; } }

     void Awake() {
        gameObject.tag = Game.coloredBlockTagLayer;
        gameObject.layer = LayerMask.NameToLayer(Game.coloredBlockTagLayer);
        modelRenderer = this.GetComponent<Renderer>();
        meshCollider = this.GetComponent<MeshCollider>();

        

        SetVisible(isVisible);
    }

    void Start() {
        
    }

    void OnTriggerEnter(Collider other) {
        Player p = other.GetComponent<Player>();

        if (p != null) {
            if (p.Glow == glowColor) {
                SetVisible(true);
            }
        }

    }

    void OnTriggerExit(Collider other) {
        Player activePlayer = Game.ActivePlayer;
        if (other.gameObject == activePlayer.gameObject) {
            if (activePlayer.Glow == glowColor) {
                SetVisible(false);
            }
        }
    }

    //private void CheckIfPlayerColorAbove() {
    //    RaycastHit hitInfo;
    //    List<Player> players = Game.Players;
    //    for (int i = 0; i < players.Count; i++) {
    //        if (players[i].Glow == glowColor) {
    //            if (Physics.Raycast(players[i].transform.position, Vector3.down, out hitInfo, 100, 1 << LayerMask.NameToLayer(Game.coloredBlockLayer))) {
    //                if (hitInfo.collider.gameObject == this.gameObject) {
    //                    SetVisible(true);
    //                    break;
    //                }
    //            }
    //        }
    //    }
    //}

    public void SetVisible(bool value) {
        isVisible = value;

        modelRenderer.enabled = isVisible;
        //meshCollider.enabled = isVisible;

        OnColorChanged.Invoke(isVisible);

        if (isVisible) {
            
        }
        else {
            List<Player> players = Game.Players;
            RaycastHit hitInfo;
            for (int i = 0; i < players.Count; i++) {
                if (Physics.Raycast(players[i].transform.position, Vector3.down, out hitInfo, 100, 1 << LayerMask.NameToLayer(Game.coloredBlockTagLayer))) {
                    if (hitInfo.collider.gameObject == this.gameObject) {
                        if (players[i].Glow == glowColor) {
                            SetVisible(true);
                        }
                        else {
                            players[i].Respawn();
                        }
                    }
                }
                    
            }
        }
    }

    void Update() {
        
    }

}
