﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Exclamation : MonoBehaviour {

    private float scaleDuration = 0.5f;
    private float rotateDuration = 4f;
    private float jumpDuration = 5;

    public Transform mainContainer;
    public Transform top;
    public Transform bottom;

    void Awake() {
        jumpDuration = Random.Range(4, 7);

        SetActive(false);
    }

	// Use this for initialization
	void Start () {
        
	}

    public void SetActive(bool value) {
        if (value) {
            mainContainer.gameObject.SetActive(true);


            bottom.DOScale(new Vector3(1.4f, 0.6f, 1.4f), scaleDuration).SetEase(Ease.Linear).OnComplete(OnBottomScaleFatComplete);

            StartCoroutine("ProcessRotate");
        }
        else {
            mainContainer.gameObject.SetActive(false);
            StopCoroutine("ProcessRotate");
            bottom.DOKill();
            mainContainer.DOKill();
        }
    }

    private void OnBottomScaleFatComplete() {
        bottom.DOScale(new Vector3(0.8f, 1.4f, 0.8f), scaleDuration).SetEase(Ease.Linear).OnComplete(OnBottomScaleThinComplete);

        mainContainer.DOJump(transform.position, jumpDuration, 1, 1).SetEase(Ease.Linear).OnComplete(OnJumpLanded);
    }

    private void OnBottomScaleThinComplete() {
        bottom.DOScale(Vector3.one, scaleDuration).SetEase(Ease.Linear);
    }

    private void OnJumpLanded() {
        bottom.DOScale(new Vector3(1.4f, 0.6f, 1.4f), scaleDuration).SetEase(Ease.Linear).OnComplete(OnBottomScaleFatComplete);
    }

    private IEnumerator ProcessRotate() {
        WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();
        float timeTaken = 0;
        float rate = 1.0f / rotateDuration;

        while (timeTaken < 1f) {
            timeTaken += Time.deltaTime * rate;

            float y = 360 * timeTaken;
            Vector3 rot = new Vector3(0, y, 0);
            top.transform.rotation = Quaternion.Euler(rot);

            yield return fixedUpdateWait;
        }

        StartCoroutine("ProcessRotate");
    }

	// Update is called once per frame
	void Update () {
	
	}
}
