﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    #region SINGLETON
    private static GameManager instance = null;
    private static bool isShuttingDown;
    private static bool hasBeenAwaked = false;
    #endregion SINGLETON

    public static GameManager Instance {
        get {
            // If there is no instance for GameManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and instance in the scene
                instance = GameObject.FindObjectOfType<GameManager>();
                //If no instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/GameManager"));
                    singleton.name = "GameManager";
                    instance = singleton.GetComponent<GameManager>();
                }
                //Set the instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an instance, either that we found or that we created.
            return instance;
        }
    }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    [SerializeField]
    private bool isPaused = false;
    [SerializeField]
    private bool isDebug = true;

    private LevelManager levelManager;

    private LevelUIController levelUIController;

    public bool IsPaused { get { return isPaused; } }
    public bool IsDebug { get { return isDebug; } }

    
    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }

        SingletonAwake();
    }

    private void SingletonAwake() {
        if (!hasBeenAwaked) {
            hasBeenAwaked = true;

            // DO THINGS
            if (isPaused) {
                Debug.LogWarning("GameManager is starting with isPaused being TRUE. That should not be...");
            }
        }
    }

	// Use this for initialization
	void Start () {
        // Can't pause or do other level related things here, cause this will exist before a level.
	}

    #region LEVEL_ONLY
    public void Pause() {
        if (!isPaused) {
            isPaused = true;
            SetPausedElsewhere();
        }
    }

    public void Unpause() {
        if (isPaused) {
            isPaused = false;
            SetPausedElsewhere();
        }
    }

    public void TogglePause() {
        isPaused = !isPaused;
        SetPausedElsewhere();
    }

    private void SetPausedElsewhere() {
        levelUIController.SetPause(isPaused);

        levelManager.SetPause(isPaused);
    }

    /// <summary>
    /// The Game UI controller is not a singleton. So it will register itself with the game manager at the start of a level.
    /// </summary>
    public void RegisterLevelUI(LevelUIController controller) {
        levelUIController = controller;

        //levelUIController.SetPause(isPaused);
    }

    public void RegisterLevelManager(LevelManager lm) {
        levelManager = lm;

        //levelManager.SetPause(isPaused);
    }

    public void SelectPlayer(GlowColor color) {
        levelManager.SelectPlayer(color);
    }

    public void ShowPlayerTurnElement(bool value) {
        //TogglePause();
        levelManager.ShowPlayerTurnElement(value);
    }

    public void ResetAllPlayers() {
        levelManager.ResetAllPlayers();
    }

    #endregion LEVEL_ONLY

    // Update is called once per frame
	void Update () {
        
	}
}
