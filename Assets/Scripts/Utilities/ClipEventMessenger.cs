﻿using UnityEngine;
using System.Collections;
using System;

public class ClipEventMessenger : MonoBehaviour {

    public Action<string> showCallback;

	// Use this for initialization
	void Start () {
	
	}

    public void OnAnimationStringEvent(string key) {
        if (showCallback != null) {
            showCallback(key);
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
