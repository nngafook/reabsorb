﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class LevelUIController : MonoBehaviour {

    private float pauseFadeSpeed = 0.75f;

    public CanvasGroup pausedLabelCG;
    public CanvasGroup playerButtonsCG;

    void Awake() {
        GameManager.Instance.RegisterLevelUI(this);
    }

	// Use this for initialization
	void Start () {
	}

    public void RedPlayerColorPressed() {
        GameManager.Instance.SelectPlayer(GlowColor.Red);
    }

    public void GreenPlayerColorPressed() {
        GameManager.Instance.SelectPlayer(GlowColor.Green);
    }

    public void BluePlayerColorPressed() {
        GameManager.Instance.SelectPlayer(GlowColor.Blue);
    }

    public void YellowPlayerColorPressed() {
        GameManager.Instance.SelectPlayer(GlowColor.Yellow);
    }

    public void SetPause(bool isPaused) {
        playerButtonsCG.SetAlphaAndBools(isPaused);

        if (isPaused) {
            pausedLabelCG.alpha = 1;
            pausedLabelCG.DOFade(0, pauseFadeSpeed).SetLoops(-1, LoopType.Yoyo);
        }
        else {
            pausedLabelCG.DOKill();
            pausedLabelCG.alpha = 0;
        }

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
