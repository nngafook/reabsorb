﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PauseSpot : WorldElement {

    public override void Awake() {
        base.Awake();
    }

    public override void Start() {
        base.Start();
    }

    protected override void OnActivePlayerChanged(Player p) {
        base.OnActivePlayerChanged(p);
    }

    public override void SwitchActivate() {
        base.SwitchActivate();

        if (activatableBySwitch) {
            SetActivated(true);
        }
    }

    public override void SwitchDeactivate() {
        base.SwitchDeactivate();

        if (activatableBySwitch) {
            SetActivated(false);
        }
    }

    public override void Update() {
        base.Update();

        //if (isActivated && activePlayer != null) {
        //    float distance = DistanceFromActivePlayer();
        //    if (playersInside.Contains(activePlayer)) {
        //        if (distance > 1) {
        //            playersInside.Remove(activePlayer);
        //        }
        //    }
        //    else {
        //        if (distance <= 1) {
        //            PutPlayerInside();
        //        }
        //    }
        //}
        
    }
	
}
