using System;
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;

public class ShortcutToolbox : EditorWindow {

    private static Vector3 levelMinPosition;

    private List<string> scenePaths = new List<string>();

    private Texture2D windowBGTexture;
    private Texture2D lowPolyTexture;
    private Vector2 scenesScrollPosition;

    private void InitTextures() {
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.4980392f, 0.572549f, 0.6901961f, 1f));
        windowBGTexture.Apply();

        lowPolyTexture = AssetDatabase.LoadAssetAtPath("Assets/Art/EditorTextures/LowPolyBG.png", typeof(Texture2D)) as Texture2D;
    }

    void Awake() {
        Init();
        InitTextures();
    }

    void OnEnable() {
        Init();
        InitTextures();
    }

    void OnFocus() {
        Init();
        InitTextures();
    }

    private string StripAssetsPath(string fullPath) {
        string rVal = fullPath.Substring(fullPath.IndexOf("Assets"));
        return rVal;
    }

    private string StripScenesPath(string fullPath) {
        string rVal = Path.GetFileNameWithoutExtension(fullPath);
        return rVal;
    }

    private string RemoveExtension(string filename) {
        return filename.Substring(0, filename.LastIndexOf("."));
    }

    private void Init() {
        scenePaths.Clear();

        string scenesFolderPath = Application.dataPath + "/Scenes";
        DirectoryInfo scenesDirectoryPath = new DirectoryInfo(scenesFolderPath);

        FileInfo[] fileInfos = scenesDirectoryPath.GetFiles("*.*", SearchOption.AllDirectories);
        string path = "";
        //fileInfos.Sort((x, y) => string.Compare(x.Name, y.Name));
        for (int i = 0; i < fileInfos.Length; i++) {
            if (fileInfos[i].Extension == ".unity") {
                path = StripAssetsPath(fileInfos[i].FullName);
                scenePaths.Add(path);
            }
        }
    }

    [MenuItem("Window/Shortcut Toolbox")]
    public static void ShowWindow() {
        EditorWindow window = EditorWindow.GetWindow(typeof(ShortcutToolbox));
        window.titleContent.text = "Toolbox";
        window.titleContent.image = AssetDatabase.LoadAssetAtPath("Assets/Art/EditorTextures/CorgiWave.png", typeof(Texture2D)) as Texture2D;
    }


    void OnGUI() {
        if (windowBGTexture == null) {
            InitTextures();
        }
        //GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), lowPolyTexture, ScaleMode.StretchToFill);

        Color defaultLabelColor = EditorStyles.boldLabel.normal.textColor;
        GUILayout.Space(10);
        scenesScrollPosition = GUILayout.BeginScrollView(scenesScrollPosition);

        //GUI.skin.label.normal.textColor = Color.white;
        EditorStyles.boldLabel.normal.textColor = Color.white;
        GUILayout.Label("Scenes", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        for (int i = 0; i < scenePaths.Count; i++) {
            if (GUILayout.Button(StripScenesPath(scenePaths[i]))) {
                //EditorSceneManager.SaveOpenScenes();
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
                    EditorSceneManager.OpenScene(scenePaths[i]);
                }
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20);
        GUILayout.Label("Tasks", EditorStyles.boldLabel);

        Color originalnormalLabelColor = EditorStyles.label.normal.textColor;
        EditorStyles.label.normal.textColor = Color.white;
        levelMinPosition = EditorGUILayout.Vector3Field("Level Min Position", levelMinPosition);
        EditorStyles.label.normal.textColor = originalnormalLabelColor;

        if (GUILayout.Button("Align Selected Object")) {
            if (Selection.activeGameObject != null) {
                float x = (Utility.RoundToNearestNth(Selection.activeGameObject.transform.position.x, 5) / 5) * 5;
                float z = (Utility.RoundToNearestNth(Selection.activeGameObject.transform.position.z, 5) / 5) * 5;

                Selection.activeGameObject.transform.position = new Vector3(x, Selection.activeGameObject.transform.position.y, z);
            }
        }

        //if (SceneManager.GetActiveScene().name == SceneName.LOADOUT) {
        //    DrawLoadoutTasks();
        //}
        //else if (SceneManager.GetActiveScene().name == SceneName.COMBAT) {
        //    DrawCombatTasks();
        //}
        //else if (SceneManager.GetActiveScene().name == SceneName.DIALOGUE) {
        //    DrawDataObjectOptions();
        //}
        //else if (SceneManager.GetActiveScene().name == SceneName.MAIN_MENU) {
        //    DrawDataObjectOptions();
        //}

        GUILayout.EndScrollView();

        //GUI.skin.label.normal.textColor = defaultLabelColor;
        EditorStyles.boldLabel.normal.textColor = defaultLabelColor;

        if (GUI.changed) {
            if (!EditorSceneManager.GetActiveScene().isDirty) {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
        }
    }
}