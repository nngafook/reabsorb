﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

public class PlayerChangeEvent : UnityEvent<Player> { }

public static class Game {

    public const string groundTagLayer = "Ground";
    public const string coloredBlockTagLayer = "ColoredBlock";
    public const string worldBottomTagLayer = "WorldBottom";
    public const string playerTagLayer = "Player";
    public const string worldElementTagLayer = "WorldElement";

    public static LevelManager levelManager;

    public static bool Debug { get { return GameManager.Instance.IsDebug; } }
    public static bool IsPaused { get { return GameManager.Instance.IsPaused; } }
    public static Player ActivePlayer { get { return levelManager.ActivePlayer; } }
    public static List<Player> Players { get { return levelManager.Players; } }


    public static void RegisterLevelManager(LevelManager lm) {
        levelManager = lm;
        //RegisterPlayerChangeListener(OnPlayerChanged);
    }

    public static void RegisterPlayerChangeListener(UnityAction<Player> callback) {
        levelManager.OnActivePlayerChanged.AddListener(callback);
    }

}

