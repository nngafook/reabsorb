﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class TurnElement : MonoBehaviour {

    //[HideInInspector]
    //public UnityEvent OnHideCompleteEvent = new UnityEvent();

    //private List<Player> playersInside = new List<Player>();

    private bool arrowPressed = false;

    [SerializeField]
    private bool rightPath = false;
    [SerializeField]
    private bool leftPath = false;
    [SerializeField]
    private bool forwardPath = false;
    [SerializeField]
    private bool backPath = false;

    public ArrowButton forwardButton;
    public ArrowButton backButton;
    public ArrowButton leftButton;
    public ArrowButton rightButton;

    public bool RightPath { get { return rightPath; } }
    public bool LeftPath { get { return leftPath; } }
    public bool ForwardPath { get { return forwardPath; } }
    public bool BackPath { get { return backPath; } }

    public bool HasAPath { get { return (rightPath || leftPath || backPath || forwardPath); } }

    void Awake() {
        forwardButton.RegisterTurnElement(this);
        backButton.RegisterTurnElement(this);
        leftButton.RegisterTurnElement(this);
        rightButton.RegisterTurnElement(this);
    }

	// Use this for initialization
    void Start() {

	}

    private IEnumerator AnimateButtonShow() {

        WaitForSeconds wait = new WaitForSeconds(0.025f);
        if (forwardPath) {
            forwardButton.Show();
            yield return wait;
        }
        if (backPath) {
            backButton.Show();
            yield return wait;
        }
        if (leftPath) {
            leftButton.Show();
            yield return wait;
        }
        if (rightPath) {
            rightButton.Show();
        }
    }

    /// <summary>
    /// Puts the turnElement on top of the player passed in
    /// Used only for the playerElements
    /// </summary>
    /// <param name="p"></param>
    public void PutOnPlayer(Player p) {
        transform.position = p.transform.position;

        SetPaths(p.CheckAvailableDirections());
    }
    
    /// <summary>
    /// Sets the buttons showing without animation
    /// </summary>
    public void SetShowButtons() {
        forwardButton.SetShow();
        backButton.SetShow();
        leftButton.SetShow();
        rightButton.SetShow();
    }

    /// <summary>
    /// Sets the buttons hidden without animation
    /// </summary>
    public void SetHideButtons() {
        forwardButton.SetHide();
        backButton.SetHide();
        leftButton.SetHide();
        rightButton.SetHide();
    }

    public void ShowButtons() {
        SetHideButtons();
        StartCoroutine(AnimateButtonShow());
    }

    public void HideButtons() {
        bool listenerAdded = false;
        if (forwardPath) {
            forwardButton.Hide();
            if (!listenerAdded) {
                listenerAdded = true;
                forwardButton.OnHideCompleteEvent.AddListener(OnButtonsHideComplete);
            }
        }
        if (backPath) {
            backButton.Hide();
            if (!listenerAdded) {
                listenerAdded = true;
                backButton.OnHideCompleteEvent.AddListener(OnButtonsHideComplete);
            }
        }
        if (leftPath) {
            leftButton.Hide();
            if (!listenerAdded) {
                listenerAdded = true;
                leftButton.OnHideCompleteEvent.AddListener(OnButtonsHideComplete);
            }
        }
        if (rightPath) {
            rightButton.Hide();
            if (!listenerAdded) {
                listenerAdded = true;
                rightButton.OnHideCompleteEvent.AddListener(OnButtonsHideComplete);
            }
        }
    }

    private void OnButtonsHideComplete(ArrowButton btn) {
        btn.OnHideCompleteEvent.RemoveListener(OnButtonsHideComplete);

        Player activePlayer = Game.ActivePlayer;
        activePlayer.StartMoving();

        arrowPressed = false;
    }

    /// <summary>
    /// Set the paths depending on the vector passed in.
    /// The Vector passed in is the forward direction of the player.
    /// Should be a normalized vector (eg: (0, 0, 1))
    /// </summary>
    /// <param name="fwdDirection"></param>
    public void SetPaths(Vector3 fwdDirection) {
        forwardPath = ((int)fwdDirection.z == -1);
        backPath = ((int)fwdDirection.z == 1);
        rightPath = ((int)fwdDirection.x == -1);
        leftPath = ((int)fwdDirection.x == 1);
    }

    /// <summary>
    /// Sets the paths available based on all four directions
    /// directions[0] fwd, directions[1] right, directions[2] back, directions[3] left
    /// </summary>
    /// <param name="directions"></param>
    public void SetPaths(char[] directions) {
        forwardPath = (directions[0] == '1');
        rightPath = (directions[1] == '1');
        backPath = (directions[2] == '1');
        leftPath = (directions[3] == '1');
    }

    public void DirectionPressed(Vector3 arrowPos) {
        if (!arrowPressed) {
            arrowPressed = true;

            Player activePlayer = Game.ActivePlayer;

            // NOTE: Player is told to start moving in OnButtonsHideComplete()
            Vector3 dir = (arrowPos - transform.position).normalized;
            activePlayer.TurnTo(dir, false);

            HideButtons();

            GameManager.Instance.Unpause();

        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
