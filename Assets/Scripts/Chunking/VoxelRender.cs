﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class VoxelRender : MonoBehaviour {
    
    private Mesh mesh;
    private List<Vector3> vertices;
    private List<int> triangles;

    private float scale = 1;
    private float adjScale = 1f;

    void Awake() {
        mesh = GetComponent<MeshFilter>().mesh;
        adjScale = scale * 0.5f;
    }

	// Use this for initialization
	void Start () {
        GenerateVoxelMesh(new VoxelData());
        UpdateMesh();
	}

    private void GenerateVoxelMesh(VoxelData voxelData) {
        vertices = new List<Vector3>();
        triangles = new List<int>();

        for (int z = 0; z < voxelData.Depth; z++) {
            for (int x = 0; x < voxelData.Width; x++) {
                if (voxelData.GetCell(x, z) == 0) {
                    continue;
                }
                MakeCube(adjScale, new Vector3((float)x * scale, 0, (float)z * scale), x, z, voxelData);
            }
        }
    }

    private void MakeCube(float cubeScale, Vector3 cubePos, int x, int z, VoxelData data) {
        for (int i = 0; i < 6; i++) {
            if (data.GetNeighbour(x, z, (Direction)i) == 0) {
                MakeFace((Direction)i, cubeScale, cubePos);
            }
        }
    }

    private void MakeFace(Direction dir, float faceScale, Vector3 facePos) {
        vertices.AddRange(CubeMeshData.FaceVerticies(dir, faceScale, facePos));
        int vCount = vertices.Count;

        triangles.Add(vCount - 4);
        triangles.Add(vCount - 4 + 1);
        triangles.Add(vCount - 4 + 2);
        triangles.Add(vCount - 4);
        triangles.Add(vCount - 4 + 2);
        triangles.Add(vCount - 4 + 3);
    }

    private void UpdateMesh() {
        mesh.Clear();

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
    }

	
	// Update is called once per frame
	void Update () {
	
	}
}
