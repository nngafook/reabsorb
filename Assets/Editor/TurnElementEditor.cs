﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TurnElement))]
public class TurnElementEditor : Editor {

    public float arrowSize = 5;

    void OnSceneGUI() {
        TurnElement turnElement = target as TurnElement;

        if (turnElement.LeftPath) {
            Handles.color = Color.red;
            Vector3 dir = (turnElement.leftButton.transform.position - turnElement.transform.position).normalized;
            Handles.DrawLine(turnElement.transform.position, turnElement.transform.position + dir * 10);
            //Handles.ArrowCap(0, turnElement.transform.position, Quaternion.Euler(0, 270, 0), arrowSize);
        }

        if (turnElement.RightPath) {
            Handles.color = Color.blue;
            Vector3 dir = (turnElement.rightButton.transform.position - turnElement.transform.position).normalized;
            Handles.DrawLine(turnElement.transform.position, turnElement.transform.position + dir * 10);
            //Handles.ArrowCap(0, turnElement.transform.position, Quaternion.Euler(0, 90, 0), arrowSize);
        }

        if (turnElement.BackPath) {
            Handles.color = Color.black;
            Vector3 dir = (turnElement.backButton.transform.position - turnElement.transform.position).normalized;
            Handles.DrawLine(turnElement.transform.position, turnElement.transform.position + dir * 10);
            //Handles.ArrowCap(0, turnElement.transform.position, Quaternion.Euler(0, 180, 0), arrowSize);
        }

        if (turnElement.ForwardPath) {
            Handles.color = Color.white;
            Vector3 dir = (turnElement.forwardButton.transform.position - turnElement.transform.position).normalized;
            Handles.DrawLine(turnElement.transform.position, turnElement.transform.position + dir * 10);
            //Handles.ArrowCap(0, turnElement.transform.position, Quaternion.Euler(0, 0, 0), arrowSize);
        }

    }
}
