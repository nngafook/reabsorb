﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.Events;

public class PlayerMovementController : MonoBehaviour {

    [HideInInspector]
    public UnityEvent OnMoveForwardComplete = new UnityEvent();

    private bool isMoving = false;

    [Tooltip("In Seconds")]
    private float moveDuration = 0.25f;
    private float worldBlockSize = 5;

    private LayerMask groundLayerMask;
    private Player player;
    private BoxCollider modelBoxCollider;
    public Transform modelTransform;

	// Use this for initialization
	void Start () {
        modelBoxCollider = modelTransform.gameObject.AddComponent<BoxCollider>();
        modelBoxCollider.size = player.Box.size;
        modelBoxCollider.center = player.Box.center;
        modelBoxCollider.isTrigger = true;
	}

    public void SetPlayer(Player p) {
        player = p;
        groundLayerMask = player.groundLayerMask;
        moveDuration = player.MoveDuration;
    }

    private IEnumerator RotateAndBounce() {
        WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();
        float timeTaken = 0;
        float rate = 1.0f / moveDuration;
        float totalTurned = 0;

        float toTurn = 0;
        float groundHeight = 0;
        Vector3 pos = modelTransform.localPosition;
        RaycastHit hitInfo;

        while (timeTaken < 1f) {
            timeTaken += Time.deltaTime * rate;

            toTurn = (timeTaken * 90) - totalTurned;
            totalTurned += toTurn;

            // Clamp amount to be rotated to within 90 degrees
            if (totalTurned > 90) {
                toTurn -= (totalTurned - 90);
            }
            modelTransform.Rotate(toTurn, 0, 0, Space.Self);

            // Calculate the difference of the collider in the ground
            if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, 100, groundLayerMask)) {
                groundHeight = transform.position.y - hitInfo.distance;
            }
            pos = modelTransform.localPosition;
            pos.y = (groundHeight - (transform.position.y - modelBoxCollider.bounds.extents.y));
            modelTransform.localPosition = pos;

            yield return fixedUpdateWait;
        }

        // Clamp rotation to exact floats ("error" floats still occur though)
        pos = modelTransform.localRotation.eulerAngles;
        pos.x = (int)pos.x;
        pos.y = (int)pos.y;
        pos.z = (int)pos.z;
        modelTransform.localRotation = Quaternion.Euler(pos);

        // Clamp y position to 0
        pos = modelTransform.localPosition;
        pos.y = 0;
        modelTransform.localPosition = pos;
    }

    private IEnumerator Rotate() {
        WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();
        float timeTaken = 0;
        float rate = 1.0f / moveDuration;
        float totalTurned = 0;
        float toTurn = 0;
        while (timeTaken < 1f) {
            timeTaken += Time.deltaTime * rate;

            toTurn = (timeTaken * 90) - totalTurned;
            totalTurned += toTurn;

            // Clamp amount to be rotated to within 90 degrees
            if (totalTurned > 90) {
                toTurn -= (totalTurned - 90);
            }
            modelTransform.Rotate(toTurn, 0, 0, Space.Self);
            yield return fixedUpdateWait;
        }

        // Clamp rotation to exact floats ("error" floats still occur though)
        Vector3 pos = modelTransform.localRotation.eulerAngles;
        pos.x = (int)pos.x;
        pos.y = (int)pos.y;
        pos.z = (int)pos.z;
        modelTransform.localRotation = Quaternion.Euler(pos);
    }

    private IEnumerator MoveForward() {
        isMoving = true;

        WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();
        Vector3 pos = transform.position;
        Vector3 playerFwd = player.NormalizedForward();

        float timeTaken = 0;
        float rate = 1.0f / moveDuration;
        float totalMoved = 0;
        float toMove = 0;
        while (timeTaken < 1f) {
            timeTaken += Time.deltaTime * rate;

            toMove = (timeTaken * worldBlockSize) - totalMoved;
            totalMoved += toMove;

            // Clamp amount to be moved to be within block size
            if (totalMoved > worldBlockSize) {
                toMove -= (totalMoved - worldBlockSize);
            }

            transform.position += (playerFwd * toMove);

            yield return fixedUpdateWait;
        }

        transform.position = pos + (playerFwd * worldBlockSize);
        isMoving = false;

        OnMoveForwardComplete.Invoke();
    }

    private IEnumerator RollForward() {
        isMoving = true;

        WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();
        Vector3 pos = transform.position;
        Vector3 playerFwd = player.NormalizedForward();

        float timeTaken = 0;
        float rate = 1.0f / moveDuration;
        float totalMoved = 0;
        float toMove = 0;
        float totalTurned = 0;
        float toTurn = 0;

        while (timeTaken < 1f) {
            timeTaken += Time.deltaTime * rate;

            // Move
            toMove = (timeTaken * worldBlockSize) - totalMoved;
            totalMoved += toMove;
            // Clamp amount to be moved to be within block size
            if (totalMoved > worldBlockSize) {
                toMove -= (totalMoved - worldBlockSize);
            }

            transform.position += (playerFwd * toMove);

            // Rotate
            toTurn = (timeTaken * 90) - totalTurned;
            totalTurned += toTurn;
            // Clamp amount to be rotated to within 90 degrees
            if (totalTurned > 90) {
                toTurn -= (totalTurned - 90);
            }
            modelTransform.Rotate(toTurn, 0, 0, Space.Self);

            yield return fixedUpdateWait;
        }

        // Clamp Movement to tile size
        transform.position = pos + (playerFwd * worldBlockSize);
        isMoving = false;

        // Clamp rotation to exact floats ("error" floats still occur though)
        pos = modelTransform.localRotation.eulerAngles;
        pos.x = Utility.RoundToNearestNth(pos.x, 90);
        pos.y = Utility.RoundToNearestNth(pos.y, 90);
        pos.z = Utility.RoundToNearestNth(pos.z, 90);
        modelTransform.localRotation = Quaternion.Euler(pos);

        OnMoveForwardComplete.Invoke();
    }

    public void MoveAndBounce() {
        if (!isMoving) {
            StartCoroutine(RollForward());
        }
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.B)) {
            StartCoroutine(Rotate());
        }
        if (Input.GetKeyUp(KeyCode.N)) {
            StartCoroutine(RotateAndBounce());
        }
        if (Input.GetKeyUp(KeyCode.M)) {
            StartCoroutine(MoveForward());
        }
        if (Input.GetKeyUp(KeyCode.Slash)) {
            MoveAndBounce();
        }
	}
}
