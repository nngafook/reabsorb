﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class LevelCamera : MonoBehaviour {

    private float moveSpeed = 2;
    private float yOffset = 20;
    private Vector3 nextPosition;
    private Player targetPlayer;

    void Awake() {
        
    }

	// Use this for initialization
	void Start () {
	    
	}

    public void RegisterSelectedPlayer(Player player) {
        targetPlayer = player;
        nextPosition = new Vector3(targetPlayer.transform.position.x, targetPlayer.transform.position.y + yOffset, transform.position.z);
        transform.DOMove(nextPosition, 0.75f);
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (!GameManager.Instance.IsPaused) {
            nextPosition = new Vector3(targetPlayer.transform.position.x, targetPlayer.transform.position.y + yOffset, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, nextPosition, Time.deltaTime * moveSpeed);
        }
	}
}
