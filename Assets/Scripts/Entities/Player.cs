﻿using UnityEngine;
using System.Linq;
using DG.Tweening;
using System.Collections;

[RequireComponent(typeof(PlayerMovementController))]
public class Player : MonoBehaviour {

    private Vector3 rotateToAfterSquish;

    private float jumpTurnHeight = 2f;
    private float jumpTurnDuration = 0.35f;
    private float respawnSpeed = 0.25f;
    private float sizeDimension = 3;
    private float sizeDimensionWithPadding = 3.5f;
    private float downRaycastDistance = 1000;

    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private BoxCollider boxCollider;
    private PlayerMovementController movementController;

    public bool debug = false;

    [SerializeField]
    private GlowColor glowColor;
    [Tooltip("In Seconds")]
    [SerializeField]
    private float moveDuration = 0.25f;
    [SerializeField]
    private bool isActive = false;
    [SerializeField]
    private bool isMoving = false;
    //[SerializeField]
    //private bool isFalling = false;
    [SerializeField]
    private bool isTurning = false;

    public LayerMask groundLayerMask;
    public Transform groundChecker;
    public Renderer mainRenderer;
    public Renderer colorIndicator;

    public float MoveDuration { get { return moveDuration; } }
    public bool IsActive { get { return isActive; } }
    public bool IsMoving { get { return isMoving; } }
    public GlowColor Glow { get { return glowColor; } }
    public BoxCollider Box { get { return boxCollider; } }

    void Awake() {
        boxCollider = this.GetComponent<BoxCollider>();
        movementController = this.GetComponent<PlayerMovementController>();

        movementController.SetPlayer(this);
        movementController.OnMoveForwardComplete.AddListener(OnBlockMoveComplete);

        spawnPosition = transform.position;
        spawnRotation = transform.rotation;

        sizeDimensionWithPadding = sizeDimension + 1;

        colorIndicator.material.color = Utility.GetGlowColorValue(glowColor);
        SetActive(isActive);
    }

    void Start() {

    }

    private void SetMoving(bool value) {
        isMoving = value;
    }

    public Vector3 NormalizedForward() {
        return transform.rotation * Vector3.forward;
    }

    private void DebugPrintAvailableDirections(char[] directions) {
        if (debug) {
            for (int i = 0; i < directions.Length; i++) {
                string dir = "LEFT: ";
                switch (i) {
                    case 0:
                        dir = "FWD: ";
                        break;
                    case 1:
                        dir = "RIGHT: ";
                        break;
                    case 2:
                        dir = "BACK: ";
                        break;
                }
                Debug.Log(dir + ": " + directions[i]);
            }
            Debug.Log("-----");
        }
    }

    private bool CheckIfBelowIsWalkable(Vector3 pos) {
        bool rVal = false;
        RaycastHit hitInfo;

        RaycastHit[] hits;
        hits = Physics.RaycastAll(pos, Vector3.down, downRaycastDistance, groundLayerMask).OrderBy(h => h.distance).ToArray();

        for (int i = 0; i < hits.Length; i++) {
            hitInfo = hits[i];
            if (hitInfo.collider.tag == Game.worldBottomTagLayer) {
                rVal = false;
                break;
            }
            else if (hitInfo.collider.tag == Game.groundTagLayer) {
                rVal = true;
                break;
            }
            else if (hitInfo.collider.tag == Game.coloredBlockTagLayer && (hitInfo.collider.GetComponent<ColoredBlock>().IsVisible || hitInfo.collider.GetComponent<ColoredBlock>().glowColor == glowColor)) {
                rVal = true;
                break;
            }
        }

        //if (Physics.Raycast(pos, Vector3.down, out hitInfo, downRaycastDistance, groundLayerMask)) {
        //    if (hitInfo.collider.tag == Game.groundTagLayer || hitInfo.collider.tag == Game.worldElementTagLayer) {
        //        rVal = true;
        //    }
        //    else if (hitInfo.collider.tag == Game.coloredBlockTagLayer && (hitInfo.collider.GetComponent<ColoredBlock>().IsVisible || hitInfo.collider.GetComponent<ColoredBlock>().glowColor == glowColor)) {
        //        rVal = true;
        //    }
        //}
        return rVal;
    }

    /// <summary>
    /// Iterates moving the ground checker backwards till ground is found, then aligns the player to that edge.
    /// Should always find the ground that the player is currently on.
    /// </summary>
    //private void FindEdgeOfCurrentGround() {
        //bool edgeFound = false;
        //float increment = 0.025f;
        //int count = 0;
        //int iterLimit = 0;
        //Vector3 pos = groundChecker.position;
        //Vector3 fwd = transform.rotation * Vector3.forward;
        //bool fwdIsZ = (fwd.x == 0);
        //RaycastHit hitInfo;
        //while (!edgeFound && iterLimit < 60) {
        //    iterLimit++;
        //    if (fwdIsZ) {
        //        pos.z -= increment * Mathf.Sign(transform.forward.z);
        //    }
        //    else {
        //        pos.x -= increment * Mathf.Sign(transform.forward.x);
        //    }
        //    if (Physics.Raycast(pos, Vector3.down, out hitInfo, 100, groundLayerMask)) {
        //        count++;
        //        if (hitInfo.collider.tag == Game.groundLayer) {
        //            edgeFound = true;
        //        }
        //    }
        //}

        //Vector3 p = transform.position;
        //if (fwdIsZ) {
        //    p.z -= (count * increment) * Mathf.Sign(transform.forward.z);
        //}
        //else {
        //    p.x -= (count * increment) * Mathf.Sign(transform.forward.x);
        //}
        //transform.position = p;
    //}

    /// <summary>
    /// Returns a 4 index char[], consisting of 0's and 1's (I need to learn bit wise manipulation)
    /// [0] = fwd [1] = right [2] = back [3] = left
    /// </summary>
    /// <returns></returns>
    public char[] CheckAvailableDirections() {
        char[] rVal = new char[] { '0', '0', '0', '0' };
        rVal[0] = CheckIfBelowIsWalkable(transform.position + (Vector3.forward * sizeDimensionWithPadding)).AsInt().ToString()[0];
        rVal[1] = CheckIfBelowIsWalkable(transform.position + (Vector3.right * sizeDimensionWithPadding)).AsInt().ToString()[0];
        rVal[2] = CheckIfBelowIsWalkable(transform.position + (Vector3.back * sizeDimensionWithPadding)).AsInt().ToString()[0];
        rVal[3] = CheckIfBelowIsWalkable(transform.position + (Vector3.left * sizeDimensionWithPadding)).AsInt().ToString()[0];
        DebugPrintAvailableDirections(rVal);
        return rVal;
    }

    /// <summary>
    /// Returns number of available directions. Typically should always return at least one, backward.
    /// Important to see if there's an alternate path besides turning around.
    /// </summary>
    /// <returns></returns>
    public int TotalAvailableDirections() {
        char[] directions = CheckAvailableDirections();
        int amountFound = 0;
        for (int i = 0; i < directions.Length; i++) {
            if (directions[i] == '1') {
                amountFound++;
            }
        }
        return amountFound;
    }

    /// <summary>
    /// Returns a char array of available directions
    /// [0] = fwd [1] = right [2] = back [3] = left
    /// </summary>
    /// <returns></returns>
    public char[] GetCurrentCharDirection() {
        char[] rVal = new char[] { '0', '0', '0', '0' };
        Vector3 fwd = NormalizedForward();
        rVal[0] = ((int)fwd.z == 1).AsInt().ToString()[0];
        rVal[1] = ((int)fwd.x == 1).AsInt().ToString()[0];
        rVal[2] = ((int)fwd.z == -1).AsInt().ToString()[0];
        rVal[3] = ((int)fwd.x == -1).AsInt().ToString()[0];
        DebugPrintAvailableDirections(rVal);
        return rVal;
    }

    /// <summary>
    /// Returns the index of a char array based on the current forward of the player
    /// The return is based on the global directions, but the player directions are local
    /// </summary>
    /// <returns></returns>
    public int ForwardCharIndex() {
        Vector3 fwd = NormalizedForward();
        int rVal = -1;
        if ((int)fwd.z == 1) {
            rVal = 0;
        }
        else if ((int)fwd.z == -1) {
            rVal = 2;
        }
        else if ((int)fwd.x == 1) {
            rVal = 1;
        }
        else if ((int)fwd.x == -1) {
            rVal = 3;
        }
        return rVal;
    }

    /// <summary>
    /// Returns the index of a char array based on the current back of the player
    /// The return is based on the global directions, but the player directions are local
    /// </summary>
    /// <returns></returns>
    public int BackCharIndex() {
        Vector3 fwd = NormalizedForward();
        int rVal = -1;
        if ((int)fwd.z == 1) {
            rVal = 2;
        }
        else if ((int)fwd.z == -1) {
            rVal = 0;
        }
        else if ((int)fwd.x == 1) {
            rVal = 3;
        }
        else if ((int)fwd.x == -1) {
            rVal = 1;
        }
        return rVal;
    }

    /// <summary>
    /// Based on the index passed in, a normalized vector is returned.
    /// This is based on the char direction array. [0] = fwd [1] = right [2] = back [3] = left
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public Vector3 NormalizedVectorFromCharIndex(int index) {
        Vector3 rVal = Vector3.zero;

        switch (index) {
            case 0:
                rVal = new Vector3(0, 0, 1);
                break;
            case 1:
                rVal = new Vector3(1, 0, 0);
                break;
            case 2:
                rVal = new Vector3(0, 0, -1);
                break;
            case 3:
                rVal = new Vector3(-1, 0, 0);
                break;
        }

        return rVal;
    }

    /// <summary>
    /// Check for collisions in front, then in front below. If both pass, then returns true
    /// </summary>
    /// <returns></returns>
    private bool CanMoveForward() {
        RaycastHit hitInfo;
        string elementTag;
        bool canMoveForward = false;

        elementTag = GetFrontTag(out hitInfo);
        // Check if directly in front is empty
        if (string.IsNullOrEmpty(elementTag)) {
            elementTag = GetDownFrontTag(out hitInfo);
            // Check if front down is ground or world element
            if (!string.IsNullOrEmpty(elementTag)) {
                if (elementTag == Game.groundTagLayer || elementTag == Game.worldElementTagLayer) {
                    canMoveForward = true;
                }
                else if (elementTag == Game.coloredBlockTagLayer && (hitInfo.collider.GetComponent<ColoredBlock>().glowColor == this.Glow || hitInfo.collider.GetComponent<ColoredBlock>().IsVisible)) {
                    canMoveForward = true;
                }
            }
        }
        return canMoveForward;
    }

    /// <summary>
    /// If there are two paths available, and one is backward, turn to face the other direction.
    /// If there are more than two paths available, this will return the first available in the clockwise check
    /// </summary>
    private void TurnToAvailableDirectionNotBackward() {
        Vector3 turnDir = Vector3.zero;
        char[] directions = CheckAvailableDirections();
        int backIndex = BackCharIndex();
        for (int i = 0; i < directions.Length; i++) {
            if (directions[i] == '1' && i != backIndex) {
                turnDir = NormalizedVectorFromCharIndex(i);
                break;
            }
        }

        TurnTo(turnDir);
    }

    /// <summary>
    /// Fires the rotate coroutine. ONLY rotates, does no further movements or checks
    /// </summary>
    /// <param name="turnDir"></param>
    public void RotateTo(Vector3 turnDir) {
        StartCoroutine(ProcessRotateTo(turnDir));
    }

    public void LandFromJump() {
        RaycastHit hitInfo;
        string elementTag = GetDownTag(out hitInfo);

        if (elementTag == Game.worldElementTagLayer && hitInfo.collider.GetComponent<WorldElement>().IsActivated) {
            hitInfo.collider.GetComponent<WorldElement>().Trigger();
        }
        else {
            StopMoving();
            GameManager.Instance.Pause();
        }
    }

    public void Respawn() {
        transform.DOScale(0, respawnSpeed).SetEase(Ease.InBack).OnComplete(OnRespawnScaleDownComplete);
    }

    private void OnRespawnScaleDownComplete() {
        transform.position = spawnPosition;
        transform.rotation = spawnRotation;

        transform.DOScale(1, respawnSpeed).SetEase(Ease.OutBack);
    }

    /// <summary>
    /// Event callback when a move from one block to another is complete
    /// </summary>
    private void OnBlockMoveComplete() {
        if (!Game.IsPaused && isActive && isMoving && !isTurning) {
            RaycastHit hitInfo;
            string elementTag = GetDownTag(out hitInfo);
            
            // Check down if on world element
            if (elementTag == Game.worldElementTagLayer && hitInfo.collider.GetComponent<WorldElement>().IsActivated) {
                hitInfo.collider.GetComponent<WorldElement>().Trigger();
            }
            else {
                // Check if multiple paths available (if equals 2, that means the paths are backward and one other)
                if (TotalAvailableDirections() == 1) {
                    if (CanMoveForward()) {
                        movementController.MoveAndBounce();
                    }
                    else {
                        // Cannot keep moving in the same direction
                        StopMoving();
                        GameManager.Instance.Pause();
                    }
                } 
                else if (TotalAvailableDirections() > 2) {
                    // true - show arrows
                    GameManager.Instance.ShowPlayerTurnElement(true);
                }
                else {
                    // Check forward
                    if (CanMoveForward()) {
                        movementController.MoveAndBounce();
                    }
                    else {
                        // Cannot keep moving in the same direction
                        // But there is definitely ONE other path available, so find it and turn to it
                        TurnToAvailableDirectionNotBackward();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Returns the tag and hitInfo of the first collider directly in front and below the player
    /// </summary>
    /// <param name="hitInfo"></param>
    /// <returns></returns>
    private string GetDownFrontTag(out RaycastHit hitInfo) {
        string rVal = "";
        if (Physics.Raycast(transform.position + (transform.forward * sizeDimension), Vector3.down, out hitInfo, sizeDimension, groundLayerMask)) {
            rVal = hitInfo.collider.tag;
        }
        return rVal;
    }

    /// <summary>
    /// Returns the tag and hitInfo of the first collider directly below the player
    /// </summary>
    /// <param name="hitInfo"></param>
    /// <returns></returns>
    private string GetDownTag(out RaycastHit hitInfo) {
        string rVal = "";
        if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, downRaycastDistance, groundLayerMask)) {
            rVal = hitInfo.collider.tag;
        }
        return rVal;
    }

    /// <summary>
    /// Returns the tag and hitInfo of the collider directly in front of the player
    /// </summary>
    /// <returns></returns>
    private string GetFrontTag(out RaycastHit hitInfo) {
        string rVal = "";
        if (Physics.Raycast(transform.position, transform.forward, out hitInfo, sizeDimension, groundLayerMask)) {
            rVal = hitInfo.collider.tag;
        }
        return rVal;
    }

    private void CheckFrontGroundCollision() {
        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position + (transform.forward * sizeDimension), Vector3.down, out hitInfo, downRaycastDistance, groundLayerMask)) {
            switch (hitInfo.collider.tag) {
                case Game.groundTagLayer:
                    if (Mathf.Abs(hitInfo.distance) > (sizeDimensionWithPadding * 0.5f)) {
                        StopMoving();
                        GameManager.Instance.Pause();
                    }
                    break;
                case Game.coloredBlockTagLayer:
                    if (Mathf.Abs(hitInfo.distance) > (sizeDimensionWithPadding * 0.5f) || (!hitInfo.collider.GetComponent<ColoredBlock>().IsVisible && hitInfo.collider.GetComponent<ColoredBlock>().glowColor != glowColor)) {
                        StopMoving();

                        //FindEdgeOfCurrentGround();
                        GameManager.Instance.Pause();
                    }
                    break;
                case Game.worldBottomTagLayer:
                    if (TotalAvailableDirections() == 2) {
                        Vector3 turnDir = Vector3.zero;
                        char[] directions = CheckAvailableDirections();
                        int backIndex = BackCharIndex();
                        for (int i = 0; i < directions.Length; i++) {
                            if (directions[i] == '1' && i != backIndex) {
                                turnDir = NormalizedVectorFromCharIndex(i);
                                break;
                            }
                        }
                        //FindEdgeOfCurrentGround();
                        
                        TurnTo(turnDir);
                        StopMoving();
                    }
                    else {
                        if (GetDownTag(out hitInfo) == Game.groundTagLayer) {
                            StopMoving();

                            GameManager.Instance.Pause();
                        }
                        else {
                            Debug.Log("(WorldBottomLayer) Hit: " + hitInfo.collider.name);
                        }
                    }
                    break;
            }
        }
        else {
            // This probably means that the raycast has gone past the level, i.e. nothing is below the player
            StopMoving();

            //FindEdgeOfCurrentGround();
            GameManager.Instance.Pause();
        }
    }

    private void CheckFrontCollision() {
        RaycastHit hitInfo;
        // Check down front
        string collisionTag = GetDownFrontTag(out hitInfo);
        if (collisionTag == Game.groundTagLayer) {
            // Check down
            collisionTag = GetDownTag(out hitInfo);
            if (collisionTag == Game.groundTagLayer) {
                // Check front
                collisionTag = GetFrontTag(out hitInfo);
                //if (Physics.Raycast(groundChecker.position, transform.forward, out hitInfo, downRaycastDistance, groundLayerMask)) {
                if (!string.IsNullOrEmpty(collisionTag)) {
                    switch (collisionTag) {
                        case Game.groundTagLayer:
                            Debug.Log("Front was ground");
                            if (Mathf.Abs(hitInfo.distance) < (sizeDimensionWithPadding * 0.5f)) {
                                if (GetDownTag(out hitInfo) == Game.groundTagLayer) {
                                    StopMoving();

                                    GameManager.Instance.Pause();
                                }
                                else {
                                    Debug.Log("CheckFrontCollision - collisionTag = groundLayer. Below Hit: " + hitInfo.collider.name);
                                }

                                //StopMoving();

                                //GameManager.Instance.Pause();
                            }
                            break;
                        case Game.coloredBlockTagLayer:
                            Debug.Log("Front was colored block");
                            // Going to be a thing when colored blocks are in the way. Primarily, stopping the player if the colors match
                            break;
                    }
                }
            }
        }
    }

    private IEnumerator ProcessRotateTo(Vector3 turnDir) {
        WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();
        float timeTaken = 0;
        float rate = 1.0f / moveDuration;

        Vector3 start = transform.rotation.eulerAngles;
        Vector3 to = Quaternion.LookRotation(turnDir).eulerAngles;

        while (timeTaken < 1f) {
            timeTaken += Time.deltaTime * rate;
            transform.rotation = Quaternion.Euler(Vector3.Lerp(start, to, timeTaken));
            yield return fixedUpdateWait;
        }
        transform.rotation = Quaternion.Euler(to);
    }

    private IEnumerator WaitForTurnCompleteToMove() {
        while (isTurning) {
            yield return null;
        }
        StartMoving();
    }

    private void OnTurnAnimationComplete() {
        isTurning = false;
        //if (isMoving) {
        //    StartMoving();
        //}
    }

    //public bool IsOnOwnGlowColor() {
    //    bool rVal = false;
    //    RaycastHit hitInfo;
    //    if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, downRaycastDistance, groundLayerMask)) {
    //        if (hitInfo.collider.tag == Game.coloredBlockLayer && hitInfo.collider.GetComponent<ColoredBlock>().glowColor == glowColor) {
    //            rVal = true;
    //        }
    //    }
    //    return rVal;
    //}

    public void SetActive(bool value) {
        isActive = value;

        boxCollider.enabled = isActive;

        Color c = mainRenderer.material.color;
        c.a = (isActive) ? 1 : 0.35f;

        mainRenderer.material.color = c;
        mainRenderer.shadowCastingMode = (isActive) ? UnityEngine.Rendering.ShadowCastingMode.On : UnityEngine.Rendering.ShadowCastingMode.Off;

        if (isActive) {
            transform.DOScale(1, 0.35f).SetEase(Ease.OutBack);
        }
        else {
            transform.DOScale(0.5f, 0.35f).SetEase(Ease.InBack);
        }

        if (isActive) {
            GameManager.Instance.ShowPlayerTurnElement(true);
        }
    }

    /// <summary>
    /// Takes a NORMALIZED vector direction to turn to
    /// </summary>
    /// <param name="turnDir"></param>
    public void TurnTo(Vector3 turnDir, bool moveAfterTurn = true) {
        if (!isTurning && NormalizedForward() != turnDir) {
            isTurning = true;

            rotateToAfterSquish = turnDir;
            transform.DOScale(new Vector3(1.5f, 0.6f, 1.5f), 0.15f).OnComplete(OnScaleDownForJumpComplete);

            if (moveAfterTurn && isMoving) {
                StartCoroutine(WaitForTurnCompleteToMove());
            }
        }
    }

    private void OnScaleDownForJumpComplete() {
        transform.DOScale(1, jumpTurnDuration / 2);
        transform.DOJump(transform.position, jumpTurnHeight, 1, jumpTurnDuration).SetEase(Ease.InQuad).OnComplete(OnTurnAnimationComplete);
        RotateTo(rotateToAfterSquish);
    }

    public void StopMoving() {
        if (debug) {
            Debug.Log("StopMoving");
        }
        SetMoving(false);
    }

    public void StartMoving() {
        if (isTurning) {
            StartCoroutine(WaitForTurnCompleteToMove());
        }
        else {
            SetMoving(true);

            movementController.MoveAndBounce();
        }
    }

    void Update() {
        if (debug) {
            if (Input.GetKeyDown(KeyCode.J)) {
                CheckAvailableDirections();
            }
            if (Input.GetKeyUp(KeyCode.G)) {
                GetCurrentCharDirection();
            }
            if (Input.GetKeyUp(KeyCode.F)) {
                transform.DOJump(transform.position, 1, 1, moveDuration * 0.5f).OnComplete(OnTurnAnimationComplete);
            }
        }

        if (isActive) {
            Debug.DrawRay(groundChecker.position, Vector3.down * downRaycastDistance, Color.green);
            Debug.DrawRay(groundChecker.position, transform.forward * sizeDimension, Color.magenta);
            Debug.DrawRay(transform.position + (transform.forward * sizeDimension), Vector3.down * downRaycastDistance, Color.black);

            Debug.DrawRay(transform.position + (Vector3.forward * sizeDimensionWithPadding), Vector3.down * downRaycastDistance, Color.red);
            Debug.DrawRay(transform.position + (Vector3.right * sizeDimensionWithPadding), Vector3.down * downRaycastDistance, Color.green);
            Debug.DrawRay(transform.position + (Vector3.back * sizeDimensionWithPadding), Vector3.down * downRaycastDistance, Color.blue);
            Debug.DrawRay(transform.position + (Vector3.left * sizeDimensionWithPadding), Vector3.down * downRaycastDistance, Color.yellow);
        }

        //if (!Game.IsPaused && isActive && isMoving && !isTurning) {
        //    // Check forward collision
        //    CheckFrontCollision();

        //    // Check down collision
        //    CheckFrontGroundCollision();

        //}

    }

    void FixedUpdate() {
        //if (!Game.IsPaused && isActive && isMoving && !isTurning) {

        //    if (isFalling) {
        //        transform.Translate(Vector3.down * moveSpeed * Time.deltaTime, Space.Self);
        //    }
        //    else {
        //        movementController.MoveAndBounce();
        //        //transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime, Space.Self);
        //    }
        //}
    }

}
