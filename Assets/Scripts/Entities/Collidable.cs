﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

[Serializable]
public class ColliderEvent : UnityEvent<Collider> { }

public class Collidable : MonoBehaviour {

    [Tooltip("X - Width, Y - Height, Z - Depth")]
    public Vector3 blockDimension = new Vector3(10, 10, 10);
    public Vector3 totalBlocks = Vector3.one;

    public BoxCollider boxCollider;
    public GlowReceiver glowReceiver;

    [Space(20)]
    public ColliderEvent OnTriggerEnterEvent;

    public void AssignComponents() {
        if (boxCollider == null) {
            boxCollider = gameObject.AddComponent<BoxCollider>();
        }
        if (glowReceiver == null) {
            glowReceiver = (Instantiate(Resources.Load("Prefabs/GlowReceiverCube"), Vector3.zero, Quaternion.identity) as GameObject).GetComponent<GlowReceiver>();
            glowReceiver.transform.SetParent(this.transform, false);
        }
    }

    public void SetSizes() {
        if (boxCollider != null) {
            float w = totalBlocks.x * blockDimension.x;
            float h = totalBlocks.y * blockDimension.y;
            float d = totalBlocks.z * blockDimension.z;

            float x = 0;
            float y = h / 2;
            float z = 0;

            boxCollider.size = new Vector3(w, h, d);
            boxCollider.center = new Vector3(x, y, z);
        }
        else {
            Debug.Log("Box Collider was NULL");
        }

        if (glowReceiver != null) {
            float w = (totalBlocks.x * blockDimension.x) + 0.01f;
            float h = (totalBlocks.y * blockDimension.y) + 0.01f;
            float d = (totalBlocks.z * blockDimension.z) + 0.01f;

            float x = 0;
            float y = h / 2;
            float z = 0;

            glowReceiver.transform.localScale = new Vector3(w, h, d);
            glowReceiver.transform.localPosition = new Vector3(x, y, z);
        }
        else {
            Debug.Log("Glow Receiver was NULL");
        }
    }

    public void OnTriggerEnter(Collider other) {
        if (OnTriggerEnterEvent != null) {
            OnTriggerEnterEvent.Invoke(other);
        }
    }

}
