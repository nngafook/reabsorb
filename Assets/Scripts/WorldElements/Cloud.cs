﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class Cloud : MonoBehaviour {

    private bool goingUp = true;
    private float speed = 10;
    private float riseSpeed = 10;
    private float riseMax = 10;
    private float screenDistance = 500;
    private Vector3 startPosition;

    void Awake() {
        startPosition = new Vector3(-screenDistance, transform.position.y, transform.position.z);

        speed = Random.Range(20, 25);
        riseMax = Random.Range(10, 18);
        goingUp = Random.Range(0, 2).AsBool();
    }

	// Use this for initialization
	void Start () {
	
	}

    private void Reset() {
        speed = Random.Range(20, 25);
        riseMax = Random.Range(10, 18);
        transform.position = startPosition;
        goingUp = Random.Range(0, 2).AsBool();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Translate(Vector3.right * speed * Time.deltaTime, Space.World);

        if (goingUp) {
            transform.Translate(Vector3.up * riseSpeed * Time.deltaTime, Space.World);
            if (transform.position.y > (startPosition.y + riseMax)) {
                goingUp = false;
            }
        }
        else {
            transform.Translate(Vector3.down * riseSpeed * Time.deltaTime, Space.World);
            if (transform.position.y < (startPosition.y - riseMax)) {
                goingUp = true;
            }
        }

        if (transform.position.x > screenDistance) {
            Reset();
        }
	}
}
