﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

[CustomEditor(typeof(Collidable))]
[CanEditMultipleObjects]
public class CollidableEditor : Editor {

    private Collidable collidable;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        collidable = (target as Collidable);

        GUILayout.Space(10);
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("Assign Components")) {
            collidable.AssignComponents();
        }
        GUI.color = (collidable.boxCollider != null) ? CustomColor.GetColor(ColorName.YALLOW) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
        if (GUILayout.Button("Set Collider Size")) {
            collidable.SetSizes();
        }
        GUI.color = Color.white;
    }

}
