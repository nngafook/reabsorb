﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.Events;

public class ArrowButtonEvent : UnityEvent<ArrowButton> {}

public class ArrowButton : WorldButton {

    [HideInInspector]
    public ArrowButtonEvent OnHideCompleteEvent = new ArrowButtonEvent();

    private string showTrigger = "Show";
    private string hideTrigger = "Hide";
    private string idleTrigger = "Idle";
    private string hidingTrigger = "Hiding";
    private string showingTrigger = "Showing";

    private bool isShowing = false;

    private Color pressHighlightColor = new Color(0.4470588f, 0.7882353f, 0.3647059f, 1f);

    private ClipEventMessenger clipEventMessenger;

    private TurnElement turnElement;

    public Animator buttonAnimator;
    public Renderer meshRenderer;

    public override void Awake() {
        base.Awake();

        if (buttonAnimator != null) {
            buttonAnimator.transform.localScale = Vector3.zero;
            clipEventMessenger = buttonAnimator.GetComponent<ClipEventMessenger>();
            clipEventMessenger.showCallback = OnShowComplete;
        }
    }

    private void OnShowComplete(string key) {
        if (key == showTrigger) {
            buttonAnimator.Play(idleTrigger, -1, Random.Range(0.0f, 1.0f));
        }
        if (key == hideTrigger) {
            if (OnHideCompleteEvent != null) {
                OnHideCompleteEvent.Invoke(this);
            }
        }
    }

	// Use this for initialization
	public override void Start () {
	    base.Start();
	}

    public override void OnMouseUpAsButton() {
        if (isShowing) {
            base.OnMouseUpAsButton();

            transform.DOPunchScale(new Vector3(0.25f, 0.25f, 0.25f), 0.25f);
            meshRenderer.material.DOColor(pressHighlightColor, 0.125f).SetLoops(2, LoopType.Yoyo);

            turnElement.DirectionPressed(transform.position);
        }
    }

    public void RegisterTurnElement(TurnElement te) {
        turnElement = te;
    }

    public void Show() {
        if (!isShowing) {
            buttonAnimator.Play(showTrigger, -1);
            isShowing = true;
        }
    }

    public void Hide() {
        if (isShowing) {
            buttonAnimator.Play(hideTrigger, -1);
            isShowing = false;
        }
    }

    /// <summary>
    /// Sets hidden without animation
    /// </summary>
    public void SetHide() {
        if (isShowing) {
            isShowing = false;
            buttonAnimator.Play(hidingTrigger, -1);
        }
    }

    /// <summary>
    /// Sets showing without animation
    /// </summary>
    public void SetShow() {
        if (!isShowing) {
            isShowing = true;
            buttonAnimator.Play(showingTrigger, -1);
        }
    }

	// Update is called once per frame
    override public void Update() {
	
	}
}
