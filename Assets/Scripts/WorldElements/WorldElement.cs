﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class WorldElement : MonoBehaviour {

    protected Player activePlayer;
    
    public bool debug = false;

    [SerializeField]
    protected bool stopsPlayer = true;
    [SerializeField]
    [Tooltip("Used if element is connected to a switch. TRUE by default")]
    protected bool isActivated = true;
    [SerializeField]
    protected bool activatableBySwitch = false;
    [SerializeField]
    [Tooltip("If connected to and powered by a colored block.")]
    protected bool connectedToAColoredBlock = false;
    [SerializeField]
    [Tooltip("If true, this element is used ON players, and not just static in the world")]
    protected bool playerElement = false;
    [SerializeField]
    protected Player occupyingPlayer;
    [SerializeField]
    protected Exclamation exclamation;

    public bool colorActivated = false;


    [Tooltip("Footprint of the element in the world. It will be disabled if it exists on Start")]
    public GameObject footprint;
    public GameObject modelContainer;
    public ColoredBlock connectedColoredBlock;

    public bool IsActivated { get { return isActivated; } }
    public bool ActivatableBySwitch { get { return activatableBySwitch; } }

    public virtual void Awake() {
        if (footprint != null) {
            footprint.SetActive(false);
        }

        if (exclamation == null) {
            GameObject exclamationObject = Instantiate(Resources.Load("Prefabs/WorldElements/Exclamation")) as GameObject;
            exclamation = exclamationObject.GetComponent<Exclamation>();
            exclamationObject.transform.SetParent(this.transform, false);
        }

        if (connectedToAColoredBlock) {
            if (connectedColoredBlock != null) {
                modelContainer.SetActive(connectedColoredBlock.IsVisible);
                connectedColoredBlock.OnColorChanged.AddListener(OnConnectedColorBlockChanged);
            }
            else {
                Debug.LogError(gameObject.name + " is marked as connectedToAColoredBlock but does not have one assigned.");
            }
        }

        if (gameObject.tag == "Untagged") {
            gameObject.tag = Game.worldElementTagLayer;
        }
        if (gameObject.layer == LayerMask.NameToLayer("Default")) {
            gameObject.layer = LayerMask.NameToLayer(Game.worldElementTagLayer);
        }

        Game.RegisterPlayerChangeListener(OnActivePlayerChanged);
        
        SetActivated(isActivated);
    }


	// Use this for initialization
    public virtual void Start() {

	}

    protected virtual void OnActivePlayerChanged(Player p) {
        activePlayer = p;
    }

    protected virtual void OnTriggerEnter(Collider other) {
        
    }

    protected virtual void OnTriggerExit(Collider other) {

    }
    protected virtual void OnConnectedColorBlockChanged(bool visible) {
        modelContainer.SetActive(visible);
    }

    protected virtual void SetActivated(bool value) {
        isActivated = value;
        exclamation.SetActive(!value);
    }

    protected float DistanceFromActivePlayer() {
        Vector3 a = activePlayer.transform.position;
        Vector3 b = transform.position;
        b.y = a.y;
        return Mathf.Abs(Vector3.Distance(a, b));
    }

    protected virtual void PutPlayerInside() {
        if (occupyingPlayer != activePlayer) {
            activePlayer.StopMoving();

            //Vector3 pos = transform.position;
            //pos.y = activePlayer.transform.position.y;
            //activePlayer.transform.position = pos;

            occupyingPlayer = activePlayer;

            OnPlayerGrabbed();

            GameManager.Instance.Pause();
        }
    }

    protected virtual void OnPlayerGrabbed() {

    }

    protected virtual void OnPlayerLeft() {

    }

    /// <summary>
    /// Is called when a switch activates this as its target
    /// </summary>
    public virtual void SwitchActivate() {

    }

    /// <summary>
    /// Is called when a switch deactivates this as its target
    /// </summary>
    public virtual void SwitchDeactivate() {

    }

    /// <summary>
    /// When a player realizes it's on top of this element, this method if called
    /// </summary>
    public virtual void Trigger() {
        
    }

	// Update is called once per frame
	public virtual void Update () {
        #if UNITY_EDITOR
        if (activePlayer != null && debug) {
            Vector3 direction = activePlayer.transform.position - transform.position;
            Debug.DrawRay(this.transform.position, direction, Color.yellow);
        }
        #endif

        if (isActivated && activePlayer != null && stopsPlayer) {
            float distance = DistanceFromActivePlayer();
            if (occupyingPlayer == activePlayer) {
                if (distance > 1) {
                    occupyingPlayer = null;
                    if (debug) {
                        Debug.Log("Released");
                    }
                    OnPlayerLeft();
                }
            }
            else if (occupyingPlayer == null) {
                if (distance <= 1) {
                    if (debug) {
                        Debug.Log("Grabbed");
                    }
                    PutPlayerInside();
                }
            }

        }
	}
}
