﻿using UnityEngine;
using System.Collections;

public class Goal : WorldElement {

    public bool redPlayerIn = false;
    public bool greenPlayerIn = false;
    public bool bluePlayerIn = false;
    public bool yellowPlayerIn = false;

    public override void Awake() {
        base.Awake();

        this.tag = Game.worldElementTagLayer;
        this.gameObject.layer = LayerMask.NameToLayer(Game.worldElementTagLayer);
    }

    private bool PlayerColorInside(GlowColor c) {
        bool rVal = false;

        switch (c) {
            case GlowColor.Red:
                rVal = redPlayerIn;
                break;
            case GlowColor.Green:
                rVal = greenPlayerIn;
                break;
            case GlowColor.Blue:
                rVal = bluePlayerIn;
                break;
            case GlowColor.Yellow:
                rVal = yellowPlayerIn;
                break;
        }

        return rVal;
    }

    protected override void PutPlayerInside() {
        activePlayer.StopMoving();

        OnPlayerGrabbed();

        GameManager.Instance.Pause();
    }

    protected override void OnPlayerGrabbed() {
        base.OnPlayerGrabbed();
        switch (activePlayer.Glow) {
            case GlowColor.Red:
                redPlayerIn = true;
                break;
            case GlowColor.Green:
                greenPlayerIn = true;
                break;
            case GlowColor.Blue:
                bluePlayerIn = true;
                break;
            case GlowColor.Yellow:
                yellowPlayerIn = true;
                break;
        }

        if (redPlayerIn && greenPlayerIn && bluePlayerIn && yellowPlayerIn) {
            redPlayerIn = false;
            greenPlayerIn = false;
            bluePlayerIn = false;
            yellowPlayerIn = false;
            GameManager.Instance.ResetAllPlayers();
        }
    }

    protected override void OnPlayerLeft() {
        switch (activePlayer.Glow) {
            case GlowColor.Red:
                redPlayerIn = false;
                break;
            case GlowColor.Green:
                greenPlayerIn = false;
                break;
            case GlowColor.Blue:
                bluePlayerIn = false;
                break;
            case GlowColor.Yellow:
                yellowPlayerIn = false;
                break;
        }
    }

    public override void Update() {
        if (isActivated && activePlayer != null) {
            float distance = DistanceFromActivePlayer();
            if (PlayerColorInside(activePlayer.Glow)) {
                if (distance > 1) {
                    OnPlayerLeft();
                }
            }
            else {
                if (distance <= 1) {
                    PutPlayerInside();
                }
            }
        }
    }
	
}
