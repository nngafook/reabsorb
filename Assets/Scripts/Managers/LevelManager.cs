﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class LevelManager : MonoBehaviour {

    public PlayerChangeEvent OnActivePlayerChanged = new PlayerChangeEvent();

    [SerializeField]
    private List<Player> playerList;
    private Player activePlayer = null;
    private GlowColor activePlayerColor = GlowColor.Red;

    // Possibly debug?
    public Transform levelObjectsContainer;

    public TurnElement playerTurnElement;

    public List<Player> Players { get { return playerList; } }
    public Player ActivePlayer { get { return activePlayer; } }
    public GlowColor ActivePlayerColor { get { return activePlayerColor; } }

    void Awake() {
        // Safe. Does nothing but register the reference
        Game.RegisterLevelManager(this);
        // Safe. Does nothing but register the reference
        GameManager.Instance.RegisterLevelManager(this);
    }

	// Use this for initialization
	void Start () {
        SelectPlayer(GlowColor.Red);
        GameManager.Instance.Pause();
	}

    public Player GetPlayerByColor(GlowColor c) {
        Player rVal = playerList[0];
        for (int i = 0; i < playerList.Count; i++) {
            if (playerList[i].Glow == c) {
                rVal = playerList[i];
                break;
            }
        }
        return rVal;
    }

    public void SelectPlayer(GlowColor color) {
        if ((activePlayer == null) || activePlayer != null && activePlayer.Glow != color) {
            if (activePlayer != null) {
                activePlayer.SetActive(false);
            }

            activePlayer = GetPlayerByColor(color);
            activePlayerColor = color;
            activePlayer.SetActive(true);
            OnActivePlayerChanged.Invoke(activePlayer);
        }
    }


    /// <summary>
    /// Show or hide the turn element used for when the active player hits a dead end.
    /// </summary>
    /// <param name="value"></param>
    public void ShowPlayerTurnElement(bool value) {
        if (value) {
            if (activePlayer != null) {
                playerTurnElement.PutOnPlayer(activePlayer);
                playerTurnElement.ShowButtons();
            }
        }
        else {
            playerTurnElement.HideButtons();
            playerTurnElement.SetPaths(Vector3.zero);
            playerTurnElement.transform.position = Vector3.zero;
        }
    }

    public void SetPause(bool isPaused) {
        if (isPaused) {
            ShowPlayerTurnElement(true);
        }
        else {

        }
    }

    public void ResetAllPlayers() {
        for (int i = 0; i < playerList.Count; i++) {
            playerList[i].Respawn();
        }
    }

	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyUp(KeyCode.G)) {
        //    levelObjectsContainer.DOMoveY(levelObjectsContainer.position.y - 1000, 3f);
        //}
	}

}
