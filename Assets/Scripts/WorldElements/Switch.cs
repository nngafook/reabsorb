﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Switch : WorldElement {

    protected bool isSwitch = false;
    protected bool isToggleable = false;
    protected bool isToggledOn = true;

    
    [SerializeField]
    private List<WorldElement> switchTargets;

    public bool IsSwitch { get { return isSwitch; } }
    
    public bool IsToggleable { get { return isToggleable; } }
    public bool IsToggledOn { get { return isToggledOn; } }

    public override void Awake() {
        base.Awake();

        isSwitch = true;
    }

    public override void Start() {
        base.Start();
    }

    protected override void OnActivePlayerChanged(Player p) {
        base.OnActivePlayerChanged(p);
    }

    protected override void OnTriggerExit(Collider other) {
        base.OnTriggerExit(other);
    }

    protected override void OnTriggerEnter(Collider other) {
        base.OnTriggerEnter(other);
    }

    public virtual void OnSwitchHit() {
        isToggledOn = true;
        for (int i = 0; i < switchTargets.Count; i++) {
            switchTargets[i].SwitchActivate();
        }
    }

    public virtual void OnSwitchRelease() {
        isToggledOn = false;
        for (int i = 0; i < switchTargets.Count; i++) {
            switchTargets[i].SwitchDeactivate();
        }
    }

    public override void Update() {
        base.Update();
    }

}
