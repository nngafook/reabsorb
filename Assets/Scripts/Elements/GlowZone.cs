﻿using UnityEngine;
using System.Collections;

public class GlowZone : MonoBehaviour {

    [SerializeField]
    private GlowColor glowColor;
    private MeshRenderer meshRenderer;

    public float radius = 12;
    public GlowColor GlowColor { get { return glowColor; } }

    void Awake() {
        Color c = Utility.GetGlowColorValue(glowColor);
        meshRenderer = this.GetComponent<MeshRenderer>();
        meshRenderer.material.SetColor("_Solid_Color", c);
    }

	// Use this for initialization
	void Start () {

	}

    private void CheckSphere() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        for (int i = 0; i < hitColliders.Length; i++) {
            if (hitColliders[i].gameObject.tag == "GlowReceiver") {
                GlowReceiver gr = hitColliders[i].GetComponent<GlowReceiver>();
                if (gr != null && !gr.IsShowing) {
                    hitColliders[i].GetComponent<GlowReceiver>().GlowEnter(glowColor);
                }
            }
        }
    }

    public void SetVisible(bool value) {
        meshRenderer.enabled = value;
    }

    void FixedUpdate() {
        CheckSphere();
    }

}
